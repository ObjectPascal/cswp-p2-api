import { Response, Request, Router } from "express";
import Neode, { TemporalPropertyTypes } from "neode";
import { DecodeResult } from "./DecodeResult";
import moment from "moment";
import { checkExpirationStatus, decodeSession, encodeSession } from "./jwtCore";

const graphInstance = new Neode(
  "neo4j+s://76211851.databases.neo4j.io",
  "neo4j",
  "0cS9PzdJrTjUNFuvyr3nsmRiBjR6Oh22Tt_9SQEeA_E"
);

// neo4j+s://35c57753.databases.neo4j.io
// JFMgp_VKX2b6szt70VwMKaz6I0FmfewPYVueMxI_qyQ

graphInstance.model("User", {
  id: {
    primary: true,
    type: "uuid",
    required: true,
  },
  UserName: {
    type: "string",
    required: true,
  },
  PasswordHash: {
    type: "string",
    required: true,
  },
  LoginDate: {
    type: "datetime",
    required: true,
  },
});
const AuthRouter = Router();

AuthRouter.post("/", async (req: Request, res: Response) => {
  try {
    const { UserName, PasswordHash } = req.body;
    if (UserName && PasswordHash) {
      const user = await graphInstance.all(
        "User",
        { UserName, PasswordHash },
        { UserName: "ASC", id: "DESC" },
        1,
        0
      );

      if (user.length > 0) {
        const session = encodeSession("726c7017-b252-4f44-af9b-71ceb034b69c", {
          id: user.get(0).id(),
          dateCreated: new Date().getDate(),
        });

        if (session) {
          const affected = await graphInstance.cypher(
            `MATCH(u:User) WHERE ID(u) = $id SET u.LoginDate = datetime('${moment(
              new Date()
            ).format("yyy-MM-DTH:mm:ss")}') RETURN u`,
            { id: user.get(0).id() }
          );
          if (affected.records.length > 0) {
            return res.status(200).send(session);
          }
        } else {
          return res.status(404).send({
            ok: false,
            status: 404,
            message:
              "Failed to encode session. Reason: The operation yielded no results.",
          });
        }
      } else {
        return res.status(404).send({
          ok: false,
          status: 404,
          message:
            "Failed to encode session. Reason: Provided credentials yielded no results.",
        });
      }
    } else {
      return res.status(422).send({
        ok: false,
        status: 422,
        message:
          "Failed to encode session. Reason: Missing required parameters.",
      });
    }
  } catch (_e) {
    return res.status(500).send({
      ok: false,
      status: 500,
      message: `Failed to encode session. Reason: Server error ${_e.message}`,
    });
  }
});

AuthRouter.get("/status", async (req: Request, res: Response) => {
  try {
    if (req.header("X-JWT-Token")) {
      const decodedSession: DecodeResult = decodeSession(
        "726c7017-b252-4f44-af9b-71ceb034b69c",
        req.header("X-JWT-Token")
      );
      if (decodedSession) {
        if (decodedSession.type === "valid") {
          const status = checkExpirationStatus(decodedSession.session);
          return res.status(200).json({
            status,
            expires: new Date(decodedSession.session.expires),
            session: decodedSession.session,
          });
        } else {
          return res.status(401).json({
            ok: false,
            status: 401,
            message: `Failed to decode or validate authorization token. Reason: ${decodedSession.type}.`,
          });
        }
      } else {
        return res.status(404).send({
          ok: false,
          status: 404,
          message:
            "Failed to decode or validate authorization token. Reason: The operation yielded no results.",
        });
      }
    } else {
      return res.status(422).send({
        ok: false,
        status: 422,
        message:
          "Failed to decode or validate authorization token. Reason: No X-JWT-Token header provided.",
      });
    }
  } catch (_e) {
    return res.status(500).send({
      ok: false,
      status: 500,
      message: `Failed to decode or validate authorization token. Reason: Server error ${_e.message}`,
    });
  }
});
export default AuthRouter;
