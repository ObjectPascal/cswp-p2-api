import mongoose from "mongoose";

interface IInsurer {
  _entityData: object;
  FullName: string;
  CompanyName: string;
  Address: string;
  Zipcode: string;
}

interface InsurerModelInterface extends mongoose.Model<InsurerDoc> {
  build(data: IInsurer): InsurerDoc;
}

interface InsurerDoc extends mongoose.Document {
  _entityData: object;
  FullName: string;
  CompanyName: string;
  Address: string;
  Zipcode: string;
}

const schema = new mongoose.Schema(
  {
    _entityData: {
      _changedById: Number,
      LastChanged: {
        type: Date,
        default: new Date(Date.now()),
      },
    },
    FullName: {
      type: String,
      required: true,
    },
    CompanyName: {
      type: String,
      required: true,
    },
    Address: {
      type: String,
      required: true,
    },
    Zipcode: {
      type: String,
      required: true,
    },
  },
  {
    versionKey: false,
  }
);

schema.statics.build = (data: IInsurer) => {
  return new Insurer(data);
};

const Insurer = mongoose.model<InsurerDoc, InsurerModelInterface>(
  "Insurer",
  schema
);
export { Insurer };
