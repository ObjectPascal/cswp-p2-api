// tslint:disable:no-unused-expression

import { expect } from "chai";
import app from "../index";
import supertest from "supertest";

const apiUrl: string = "/api/v1/insurances";
const fakeId: string = "61b0b3359a6830143701e4f8";
const testUser: any = {
  UserName: "user1",
  PasswordHash:
    "a4e4570c263dbfe7e7b01c451f5121f025c7bba885bb3c373ba4a578b4a7dca4",
};

const testInsurance: any = {
  _insurer: "",
  _insuranceType: "",
  _insurancePolicy: "",
  PaymentPeriodMonths: 1,
  PrevCollectionDate: new Date(Date.parse("2021-12-01T12:00:00.000Z")),
  NextCollectionDate: new Date(Date.parse("2022-01-01T12:00:00.000Z")),
  Description: "Mocha Test Insurance",
};
const testInsuranceIncomplete: any = {
  PaymentPeriodMonths: 1,
  Description: "Mocha Test Insurance",
};

const testInsurer: any = {
  FullName: "Mocha Test Insurer",
  CompanyName: "Mocha",
  Address: "Mocha Test Address",
  Zipcode: "0987ZY",
};
const testInsuranceType: any = {
  TypeName: "Mocha Test InsuranceType",
  PaymentPeriodMonths: 1,
};
const testPolicyHolder: any = {
  IBAN: "NL40 12345",
  FullName: "Mocha Test Policyholder",
  Initials: "MTP",
  Email: "mocha@test.nl",
  Telephone: "+316 12345678",
  Address: "Mocha Test Address",
  Zipcode: "0987ZY",
  Lat: 123.45,
  Long: 123.45,
};
const testInsurancePolicy: any = {
  _policyHolder: "",
  PolicyNumber: 123,
  Premium: 123.45,
  DiscountPercentage: 10,
};

describe("> insurance tests", () => {
  let authToken: string = "";
  before(async () => {
    const result = await supertest(app).post("/api/auth").send(testUser);
    const { token } = result.body;
    authToken = token;

    const resultTestInsurer = await supertest(app)
      .post("/api/v1/insurers")
      .set("X-JWT-Token", authToken)
      .send(testInsurer);
    const resultTestInsuranceType = await supertest(app)
      .post("/api/v1/insurancetypes")
      .set("X-JWT-Token", authToken)
      .send(testInsuranceType);
    const resultTestPolicyHolder = await supertest(app)
      .post("/api/v1/policyholders")
      .set("X-JWT-Token", authToken)
      .send(testPolicyHolder);

    testInsurancePolicy._policyHolder = resultTestPolicyHolder.body._id;
    const resultTestInsurancePolicy = await supertest(app)
      .post("/api/v1/insurancepolicies")
      .set("X-JWT-Token", authToken)
      .send(testInsurancePolicy);

    testInsurance._insurer = resultTestInsurer.body._id;
    testInsurance._insuranceType = resultTestInsuranceType.body._id;
    testInsurance._insurancePolicy = resultTestInsurancePolicy.body._id;
  });

  after(async () => {
    const result1 = await supertest(app)
      .delete(`/api/v1/insurers/${testInsurance._insurer}`)
      .set("X-JWT-Token", authToken);
    const result2 = await supertest(app)
      .delete(`/api/v1/insurancetypes/${testInsurance._insuranceType}`)
      .set("X-JWT-Token", authToken);
    const result3 = await supertest(app)
      .delete(`/api/v1/insurancepolicies/${testInsurance._insurancePolicy}`)
      .set("X-JWT-Token", authToken);
    const result4 = await supertest(app)
      .delete(`/api/v1/policyholders/${testInsurancePolicy._policyHolder}`)
      .set("X-JWT-Token", authToken);

    return (
      result1.statusCode === 200 &&
      result2.statusCode === 200 &&
      result3.statusCode === 200 &&
      result4.statusCode === 200
    );
  });

  let insuranceId: string = "";
  it("C) should create a correct insurance with a correct id", async () => {
    const result = await supertest(app)
      .post(apiUrl)
      .set("X-JWT-Token", authToken)
      .send(testInsurance);

    expect(result.statusCode).to.equal(201);
    expect(result.headers["content-type"]).to.equal(
      "application/json; charset=utf-8"
    );

    const {
      _id,
      _entityData,
      _insurer,
      _insuranceType,
      _insurancePolicy,
      PaymentPeriodMonths,
      PrevCollectionDate,
      NextCollectionDate,
      Description,
    } = result.body;

    expect(_id).to.be.a("string").that.is.not.empty;
    insuranceId = _id;

    expect(_entityData).to.be.a("object").that.is.not.null;
    expect(_insurer).to.be.a("string").that.is.not.empty;
    expect(_insuranceType).to.be.a("string").that.is.not.empty;
    expect(_insurancePolicy).to.be.a("string").that.is.not.empty;
    expect(PaymentPeriodMonths).to.be.a("number").that.equals(1);
    expect(PrevCollectionDate)
      .to.be.a("string")
      .that.equals("2021-12-01T12:00:00.000Z");
    expect(NextCollectionDate)
      .to.be.a("string")
      .that.equals("2022-01-01T12:00:00.000Z");
    expect(Description).to.be.a("string").that.equals("Mocha Test Insurance");
  });
  it("C) should not create an insurance with incomplete data", async () => {
    const result = await supertest(app)
      .post(apiUrl)
      .set("X-JWT-Token", authToken)
      .send(testInsuranceIncomplete);

    expect(result.statusCode).to.equal(422);
    expect(result.headers["content-type"]).to.equal(
      "application/json; charset=utf-8"
    );
  });

  it("R) should provide a list of insurances", async () => {
    const result = await supertest(app)
      .get(`${apiUrl}/list`)
      .set("X-JWT-Token", authToken);

    expect(result.statusCode).to.equal(200);
    expect(result.headers["content-type"]).to.equal(
      "application/json; charset=utf-8"
    );

    expect(result.body).to.not.be.null;
    expect(result.body).to.have.length.greaterThan(0);
    expect(result.body).to.be.a("array");
  });
  it("R) should read a correct insurance", async () => {
    const result = await supertest(app)
      .get(`${apiUrl}/${insuranceId}`)
      .set("X-JWT-Token", authToken);

    expect(result.statusCode).to.equal(200);
    expect(result.headers["content-type"]).to.equal(
      "application/json; charset=utf-8"
    );

    const {
      _id,
      _entityData,
      _insurer,
      _insuranceType,
      _insurancePolicy,
      PaymentPeriodMonths,
      PrevCollectionDate,
      NextCollectionDate,
      Description,
    } = result.body;

    expect(_id).to.be.a("string").that.is.not.empty;
    expect(_entityData).to.be.a("object").that.is.not.null;
    expect(_insurer).to.be.a("object").that.is.not.null;
    expect(_insuranceType).to.be.a("object").that.is.not.null;
    expect(_insurancePolicy).to.be.a("object").that.is.not.null;
    expect(PaymentPeriodMonths).to.be.a("number").that.is.above(-1);
    expect(new Date(Date.parse(PrevCollectionDate))).to.be.a("date").that.is.not
      .null;
    expect(new Date(Date.parse(NextCollectionDate))).to.be.a("date").that.is.not
      .null;
    expect(Description).to.be.a("string").that.is.not.empty;
  });
  it("R) should not read an insurance with a false id", async () => {
    const result = await supertest(app)
      .get(`${apiUrl}/${fakeId}`)
      .set("X-JWT-Token", authToken);

    expect(result.statusCode).to.equal(404);
    expect(result.headers["content-type"]).to.equal(
      "application/json; charset=utf-8"
    );
  });

  it("U) should update a correct insurance with a correct id", async () => {
    const result = await supertest(app)
      .put(`${apiUrl}/${insuranceId}`)
      .set("X-JWT-Token", authToken)
      .send(testInsurance);

    expect(result.statusCode).to.equal(200);
    expect(result.headers["content-type"]).to.equal(
      "application/json; charset=utf-8"
    );

    const {
      _id,
      _entityData,
      _insurer,
      _insuranceType,
      _insurancePolicy,
      PaymentPeriodMonths,
      PrevCollectionDate,
      NextCollectionDate,
      Description,
    } = result.body;

    expect(_id).to.be.a("string").that.is.not.empty;
    expect(_entityData).to.be.a("object").that.is.not.null;
    expect(_insurer).to.be.a("object").that.is.not.null;
    expect(_insuranceType).to.be.a("object").that.is.not.null;
    expect(_insurancePolicy).to.be.a("object").that.is.not.null;
    expect(PaymentPeriodMonths).to.be.a("number").that.is.above(-1);
    expect(new Date(Date.parse(PrevCollectionDate))).to.be.a("date").that.is.not
      .null;
    expect(new Date(Date.parse(NextCollectionDate))).to.be.a("date").that.is.not
      .null;
    expect(Description).to.be.a("string").that.is.not.empty;
  });
  it("U) should not update an insurerance with a false id", async () => {
    const result = await supertest(app)
      .put(`${apiUrl}/${fakeId}`)
      .set("X-JWT-Token", authToken)
      .send(testInsurance);

    expect(result.statusCode).to.equal(404);
    expect(result.headers["content-type"]).to.equal(
      "application/json; charset=utf-8"
    );
  });
  it("U) should not update an insurance with incomplete data", async () => {
    const result = await supertest(app)
      .put(`${apiUrl}/${insuranceId}`)
      .set("X-JWT-Token", authToken)
      .send(testInsuranceIncomplete);

    expect(result.statusCode).to.equal(422);
  });

  it("D) should delete and return data when a correct insurance with a correct id is deleted", async () => {
    const result = await supertest(app)
      .delete(`${apiUrl}/${insuranceId}`)
      .set("X-JWT-Token", authToken);

    expect(result.statusCode).to.equal(200);
    expect(result.headers["content-type"]).to.equal(
      "application/json; charset=utf-8"
    );

    const {
      _id,
      _entityData,
      _insurer,
      _insuranceType,
      _insurancePolicy,
      PaymentPeriodMonths,
      PrevCollectionDate,
      NextCollectionDate,
      Description,
    } = result.body;

    expect(_id).to.be.a("string").that.is.not.empty;
    expect(_entityData).to.be.a("object").that.is.not.null;
    expect(_insurer).to.be.a("object").that.is.not.null;
    expect(_insuranceType).to.be.a("object").that.is.not.null;
    expect(_insurancePolicy).to.be.a("object").that.is.not.null;
    expect(PaymentPeriodMonths).to.be.a("number").that.is.above(-1);
    expect(new Date(Date.parse(PrevCollectionDate))).to.be.a("date").that.is.not
      .null;
    expect(new Date(Date.parse(NextCollectionDate))).to.be.a("date").that.is.not
      .null;
    expect(Description).to.be.a("string").that.is.not.empty;
  });
  it("D) should not delete an insurance if it is already deleted", async () => {
    const result = await supertest(app)
      .delete(`${apiUrl}/${insuranceId}`)
      .set("X-JWT-Token", authToken);

    expect(result.statusCode).to.equal(404);
    expect(result.headers["content-type"]).to.equal(
      "application/json; charset=utf-8"
    );
  });
  it("D) should not delete an insurance with a fake id", async () => {
    const result = await supertest(app)
      .delete(`${apiUrl}/${fakeId}`)
      .set("X-JWT-Token", authToken);

    expect(result.statusCode).to.equal(404);
    expect(result.headers["content-type"]).to.equal(
      "application/json; charset=utf-8"
    );
  });
});
