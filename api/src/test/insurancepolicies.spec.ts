// tslint:disable:no-unused-expression

import { expect } from "chai";
import app from "../index";
import supertest from "supertest";

const apiUrl: string = "/api/v1/insurancepolicies";
const fakeId: string = "61b0b3359a6830143701e4f8";
const testUser: any = {
  UserName: "user1",
  PasswordHash:
    "a4e4570c263dbfe7e7b01c451f5121f025c7bba885bb3c373ba4a578b4a7dca4",
};

const testInsurancePolicy: any = {
  _policyHolder: "",
  PolicyNumber: 123,
  Premium: 123.45,
  DiscountPercentage: 10,
};
const testInsurancePolicyIncomplete: any = {
  PolicyNumber: 123,
  DiscountPercentage: 10,
};
const testInsurancePolicyNonRequired: any = {
  _policyHolder: "",
  PolicyNumber: 123,
  Premium: 123.45,
};

const testPolicyHolder: any = {
  IBAN: "NL40 12345",
  FullName: "Mocha Test Policyholder",
  Initials: "MTP",
  Email: "mocha@test.nl",
  Telephone: "+316 12345678",
  Address: "Mocha Test Address",
  Zipcode: "0987ZY",
  Lat: 123.45,
  Long: 123.45,
};

describe("> insurancepolicy tests", () => {
  let authToken: string = "";
  before(async () => {
    const result = await supertest(app).post("/api/auth").send(testUser);
    const { token } = result.body;
    authToken = token;

    const resultTestPolicyHolder = await supertest(app)
      .post("/api/v1/policyholders")
      .set("X-JWT-Token", authToken)
      .send(testPolicyHolder);

    const { _id } = resultTestPolicyHolder.body;
    testInsurancePolicy._policyHolder = _id;
    testInsurancePolicyNonRequired._policyHolder = _id;
  });

  after(async () => {
    const result = await supertest(app)
      .delete(`/api/v1/policyholders/${testInsurancePolicy._policyHolder}`)
      .set("X-JWT-Token", authToken);
    return result.statusCode === 200;
  });

  let insurancePolicyId: string = "";
  let insurancePolicyIdNonRequired: string = "";
  it("C) should create a correct insurancepolicy with a correct id", async () => {
    const result = await supertest(app)
      .post(apiUrl)
      .set("X-JWT-Token", authToken)
      .send(testInsurancePolicy);

    expect(result.statusCode).to.equal(201);
    expect(result.headers["content-type"]).to.equal(
      "application/json; charset=utf-8"
    );
    const {
      _id,
      _entityData,
      _policyHolder,
      PolicyNumber,
      Premium,
      DiscountPercentage,
    } = result.body;

    expect(_id).to.be.a("string").that.is.not.empty;
    insurancePolicyId = _id;

    expect(_entityData).to.be.a("object").that.is.not.null;
    expect(_policyHolder).to.be.a("string").that.is.not.empty;
    expect(PolicyNumber).to.be.a("number").that.equals(123);
    expect(Premium).to.be.a("number").that.equals(123.45);
    expect(DiscountPercentage).to.be.a("number").that.equals(10);
  });
  it("C) should create a correct insurancepolicy with only the required parameters", async () => {
    const result = await supertest(app)
      .post(apiUrl)
      .set("X-JWT-Token", authToken)
      .send(testInsurancePolicyNonRequired);

    expect(result.statusCode).to.equal(201);
    expect(result.headers["content-type"]).to.equal(
      "application/json; charset=utf-8"
    );
    const {
      _id,
      _entityData,
      _policyHolder,
      PolicyNumber,
      Premium,
      DiscountPercentage,
    } = result.body;

    expect(_id).to.be.a("string").that.is.not.empty;
    insurancePolicyIdNonRequired = _id;

    expect(_entityData).to.be.a("object").that.is.not.null;
    expect(_policyHolder).to.be.a("string").that.is.not.empty;
    expect(PolicyNumber).to.be.a("number").that.equals(123);
    expect(Premium).to.be.a("number").that.equals(123.45);
    expect(DiscountPercentage).to.be.a("number").that.equals(0);
  });
  it("C) should not create an insurancepolicy with incomplete data", async () => {
    const result = await supertest(app)
      .post(apiUrl)
      .set("X-JWT-Token", authToken)
      .send(testInsurancePolicyIncomplete);

    expect(result.statusCode).to.equal(422);
    expect(result.headers["content-type"]).to.equal(
      "application/json; charset=utf-8"
    );
  });

  it("R) should provide a list of insurancepolicies", async () => {
    const result = await supertest(app)
      .get(`${apiUrl}/list`)
      .set("X-JWT-Token", authToken);

    expect(result.statusCode).to.equal(200);
    expect(result.headers["content-type"]).to.equal(
      "application/json; charset=utf-8"
    );

    expect(result.body).to.not.be.null;
    expect(result.body).to.have.length.greaterThan(0);
    expect(result.body).to.be.a("array");
  });
  it("R) should read a correct insurancepolicy", async () => {
    const result = await supertest(app)
      .get(`${apiUrl}/${insurancePolicyId}`)
      .set("X-JWT-Token", authToken);

    expect(result.statusCode).to.equal(200);
    expect(result.headers["content-type"]).to.equal(
      "application/json; charset=utf-8"
    );
    const {
      _id,
      _entityData,
      _policyHolder,
      PolicyNumber,
      Premium,
      DiscountPercentage,
    } = result.body;

    expect(_id).to.be.a("string").that.is.not.empty;
    expect(_entityData).to.be.a("object").that.is.not.null;
    expect(_policyHolder).to.be.a("object").that.is.not.null;
    expect(PolicyNumber).to.be.a("number").that.is.above(-1);
    expect(Premium).to.be.a("number").that.is.above(-1);
    expect(DiscountPercentage).to.be.a("number").that.is.above(-1);
  });
  it("R) should not read an insurancepolicy with a false id", async () => {
    const result = await supertest(app)
      .get(`${apiUrl}/${fakeId}`)
      .set("X-JWT-Token", authToken);

    expect(result.statusCode).to.equal(404);
    expect(result.headers["content-type"]).to.equal(
      "application/json; charset=utf-8"
    );
  });

  it("U) should update a correct insurancepolicy with a correct id", async () => {
    const result = await supertest(app)
      .put(`${apiUrl}/${insurancePolicyId}`)
      .set("X-JWT-Token", authToken)
      .send(testInsurancePolicy);

    expect(result.statusCode).to.equal(200);
    expect(result.headers["content-type"]).to.equal(
      "application/json; charset=utf-8"
    );
    const {
      _id,
      _entityData,
      _policyHolder,
      PolicyNumber,
      Premium,
      DiscountPercentage,
    } = result.body;

    expect(_id).to.be.a("string").that.is.not.empty;
    expect(_entityData).to.be.a("object").that.is.not.null;
    expect(_policyHolder).to.be.a("object").that.is.not.null;
    expect(PolicyNumber).to.be.a("number").that.is.above(-1);
    expect(Premium).to.be.a("number").that.is.above(-1);
    expect(DiscountPercentage).to.be.a("number").that.is.above(-1);
  });
  it("U) should not update an insurerancepolicy with a false id", async () => {
    const result = await supertest(app)
      .put(`${apiUrl}/${fakeId}`)
      .set("X-JWT-Token", authToken)
      .send(testInsurancePolicy);

    expect(result.statusCode).to.equal(404);
    expect(result.headers["content-type"]).to.equal(
      "application/json; charset=utf-8"
    );
  });
  it("U) should not update an insurancepolicy with incomplete data", async () => {
    const result = await supertest(app)
      .put(`${apiUrl}/${insurancePolicyId}`)
      .set("X-JWT-Token", authToken)
      .send(testInsurancePolicyIncomplete);

    expect(result.statusCode).to.equal(422);
    expect(result.headers["content-type"]).to.equal(
      "application/json; charset=utf-8"
    );
  });

  it("D) should delete a correct insurancepolicy with a correct id", async () => {
    const result = await supertest(app)
      .delete(`${apiUrl}/${insurancePolicyIdNonRequired}`)
      .set("X-JWT-Token", authToken);

    expect(result.statusCode).to.equal(200);
    expect(result.headers["content-type"]).to.equal(
      "application/json; charset=utf-8"
    );
  });
  it("D) should return data when a correct insurancepolicy with a correct id is deleted", async () => {
    const result = await supertest(app)
      .delete(`${apiUrl}/${insurancePolicyId}`)
      .set("X-JWT-Token", authToken);

    expect(result.statusCode).to.equal(200);
    expect(result.headers["content-type"]).to.equal(
      "application/json; charset=utf-8"
    );
    const {
      _id,
      _entityData,
      _policyHolder,
      PolicyNumber,
      Premium,
      DiscountPercentage,
    } = result.body;

    expect(_id).to.be.a("string").that.is.not.empty;
    expect(_entityData).to.be.a("object").that.is.not.null;
    expect(_policyHolder).to.be.a("object").that.is.not.null;
    expect(PolicyNumber).to.be.a("number").that.is.above(-1);
    expect(Premium).to.be.a("number").that.is.above(-1);
    expect(DiscountPercentage).to.be.a("number").that.is.above(-1);
  });
  it("D) should not delete an insurancepolicy if it is already deleted", async () => {
    const result = await supertest(app)
      .delete(`${apiUrl}/${insurancePolicyId}`)
      .set("X-JWT-Token", authToken);

    expect(result.statusCode).to.equal(404);
    expect(result.headers["content-type"]).to.equal(
      "application/json; charset=utf-8"
    );
  });
  it("D) should not delete an insurancepolicy with a fake id", async () => {
    const result = await supertest(app)
      .delete(`${apiUrl}/${fakeId}`)
      .set("X-JWT-Token", authToken);

    expect(result.statusCode).to.equal(404);
    expect(result.headers["content-type"]).to.equal(
      "application/json; charset=utf-8"
    );
  });
});
