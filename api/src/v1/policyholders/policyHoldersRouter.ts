import { Response, Request, Router } from "express";
import { PolicyHolder } from "./PolicyHolder";
import { getSessionBearerId } from "../../auth/jwtCore";

const policyHoldersRouter = Router();

policyHoldersRouter.get("/list", async (req: Request, res: Response) => {
  try {
    const result = await PolicyHolder.find();
    if (result) {
      return res.status(200).send(result);
    } else {
      return res.status(404).send({
        ok: false,
        status: 404,
        message:
          "Failed to retrieve a list of policyholder entities. Reason: The operation yielded no results.",
      });
    }
  } catch (_e) {
    return res.status(500).send({
      ok: false,
      status: 500,
      message: `Failed to retrieve a list of policyholder entities. Reason: Server error ${_e.message}`,
    });
  }
});
policyHoldersRouter.get("/:id", async (req: Request, res: Response) => {
  try {
    if (req.params.id) {
      const result = await PolicyHolder.findById(req.params.id);
      if (result) {
        return res.status(200).send(result);
      } else {
        return res.status(404).send({
          ok: false,
          status: 404,
          message:
            "Failed to retrieve policyholder entity. Reason: Entity does not exist.",
        });
      }
    } else {
      return res.status(422).send({
        ok: false,
        status: 422,
        message:
          "Failed to process policyholder entity. Reason: ID parameter is empty.",
      });
    }
  } catch (_e) {
    return res.status(500).send({
      ok: false,
      status: 500,
      message: `Failed to retrieve policyholder entity. Reason: Server error ${_e.message}`,
    });
  }
});
policyHoldersRouter.post("/", async (req: Request, res: Response) => {
  try {
    const {
      IBAN,
      FullName,
      Initials,
      Email,
      Telephone,
      Address,
      Zipcode,
      Lat,
      Long,
    } = req.body;
    if (
      IBAN &&
      FullName &&
      Initials &&
      Email &&
      Telephone &&
      Address &&
      Zipcode
    ) {
      const doc = PolicyHolder.build({
        _entityData: {
          _changedById: getSessionBearerId(req),
          LastChanged: new Date(),
        },
        IBAN,
        FullName,
        Initials,
        Email,
        Telephone,
        Address,
        Zipcode,
        Lat,
        Long,
      });
      const result = await doc.save();
      return res.status(201).send(result);
    } else {
      return res.status(422).send({
        ok: false,
        status: 422,
        message:
          "Failed to process policyholder entity. Reason: Missing required parameters.",
      });
    }
  } catch (_e) {
    return res.status(500).send({
      ok: false,
      status: 500,
      message: `Failed to process policyholder entity. Reason: Server error ${_e.message}`,
    });
  }
});
policyHoldersRouter.put("/:id", async (req: Request, res: Response) => {
  try {
    if (req.params.id) {
      const bearerId = getSessionBearerId(req);
      const checkObj: any = await PolicyHolder.findById(req.params.id);
      if (checkObj) {
        const { _changedById } = checkObj._entityData;
        if (bearerId === _changedById) {
          const {
            IBAN,
            FullName,
            Initials,
            Email,
            Telephone,
            Address,
            Zipcode,
            Lat,
            Long,
          } = req.body;
          if (
            IBAN &&
            FullName &&
            Initials &&
            Email &&
            Telephone &&
            Address &&
            Zipcode
          ) {
            const result = await PolicyHolder.findByIdAndUpdate(req.params.id, {
              _entityData: {
                _changedById: bearerId,
                LastChanged: new Date(),
              },
              IBAN,
              FullName,
              Initials,
              Email,
              Telephone,
              Address,
              Zipcode,
              Lat,
              Long,
            });
            if (result) {
              return res.status(200).send(result);
            } else {
              return res.status(404).send({
                ok: false,
                status: 404,
                message:
                  "Failed to process policyholder entity. Reason: Entity does not exist.",
              });
            }
          } else {
            return res.status(422).send({
              ok: false,
              status: 422,
              message:
                "Failed to process policyholder entity. Reason: Missing required parameters.",
            });
          }
        } else {
          return res.status(401).send({
            ok: false,
            status: 401,
            message:
              "Failed to process policyholder entity. Reason: Entity change rejected.",
          });
        }
      } else {
        return res.status(404).send({
          ok: false,
          status: 404,
          message:
            "Failed to process policyholder entity. Reason: Unable to check entity isolation since the entity does not exist.",
        });
      }
    } else {
      return res.status(422).send({
        ok: false,
        status: 422,
        message:
          "Failed to process policyholder entity. Reason: ID parameter is empty.",
      });
    }
  } catch (_e) {
    return res.status(500).send({
      ok: false,
      status: 500,
      message: `Failed to process policyholder entity. Reason: Server error ${_e.message}`,
    });
  }
});
policyHoldersRouter.delete("/:id", async (req: Request, res: Response) => {
  try {
    if (req.params.id) {
      const bearerId = getSessionBearerId(req);
      const checkObj: any = await PolicyHolder.findById(req.params.id);
      if (checkObj) {
        const { _changedById } = checkObj._entityData;
        if (bearerId === _changedById) {
          const result = await PolicyHolder.findByIdAndRemove(req.params.id);
          if (result) {
            return res.status(200).send(result);
          } else {
            return res.status(404).send({
              ok: false,
              status: 404,
              message:
                "Failed to process policyholder entity. Reason: Entity does not exist.",
            });
          }
        } else {
          return res.status(401).send({
            ok: false,
            status: 401,
            message:
              "Failed to process policyholder entity. Reason: Entity change rejected.",
          });
        }
      } else {
        return res.status(404).send({
          ok: false,
          status: 404,
          message:
            "Failed to process policyholder entity. Reason: Unable to check entity isolation since the entity does not exist.",
        });
      }
    } else {
      return res.status(422).send({
        ok: false,
        status: 422,
        message:
          "Failed to process policyholder entity. Reason: ID parameter is empty.",
      });
    }
  } catch (_e) {
    return res.status(500).send({
      ok: false,
      status: 500,
      message: `Failed to process policyholder entity. Reason: Server error ${_e.message}`,
    });
  }
});
export default policyHoldersRouter;
