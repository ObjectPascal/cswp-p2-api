// tslint:disable:no-unused-expression

import { expect } from "chai";
import app from "../index";
import supertest from "supertest";

const apiUrl: string = "/api/v1/insurers";
const fakeId: string = "61b0b3359a6830143701e4f8";
const testUser: any = {
  UserName: "user1",
  PasswordHash:
    "a4e4570c263dbfe7e7b01c451f5121f025c7bba885bb3c373ba4a578b4a7dca4",
};

const testInsurer: any = {
  FullName: "Mocha Test Insurer",
  CompanyName: "Mocha",
  Address: "Mocha Test Address",
  Zipcode: "0987ZY",
};
const testInsurerIncomplete: any = {
  Zipcode: "0987ZY",
};

describe("> insurer tests", () => {
  let authToken: string = "";
  before(async () => {
    const result = await supertest(app).post("/api/auth").send(testUser);
    const { token } = result.body;
    authToken = token;
  });

  let insurerId: string = "";
  it("C) should create a correct insurer with a correct id", async () => {
    const result = await supertest(app)
      .post(apiUrl)
      .set("X-JWT-Token", authToken)
      .send(testInsurer);

    expect(result.statusCode).to.equal(201);
    expect(result.headers["content-type"]).to.equal(
      "application/json; charset=utf-8"
    );

    const { _id, _entityData, FullName, CompanyName, Address, Zipcode } =
      result.body;

    expect(_id).to.be.a("string").that.is.not.empty;
    insurerId = _id;

    expect(_entityData).to.be.a("object").that.is.not.null;
    expect(FullName).to.be.a("string").that.equals("Mocha Test Insurer");
    expect(CompanyName).to.be.a("string").that.equals("Mocha");
    expect(Address).to.be.a("string").that.equals("Mocha Test Address");
    expect(Zipcode).to.be.a("string").that.equals("0987ZY");
  });
  it("C) should not create an insurer with incomplete data", async () => {
    const result = await supertest(app)
      .post(apiUrl)
      .set("X-JWT-Token", authToken)
      .send(testInsurerIncomplete);

    expect(result.statusCode).to.equal(422);
    expect(result.headers["content-type"]).to.equal(
      "application/json; charset=utf-8"
    );
  });

  it("R) should provide a list of insurers", async () => {
    const result = await supertest(app)
      .get(`${apiUrl}/list`)
      .set("X-JWT-Token", authToken);

    expect(result.statusCode).to.equal(200);
    expect(result.headers["content-type"]).to.equal(
      "application/json; charset=utf-8"
    );

    expect(result.body).to.not.be.null;
    expect(result.body).to.have.length.greaterThan(0);
    expect(result.body).to.be.a("array");
  });
  it("R) should read a correct insurer", async () => {
    const result = await supertest(app)
      .get(`${apiUrl}/${insurerId}`)
      .set("X-JWT-Token", authToken);

    expect(result.statusCode).to.equal(200);
    expect(result.headers["content-type"]).to.equal(
      "application/json; charset=utf-8"
    );

    const { _id, _entityData, FullName, CompanyName, Address, Zipcode } =
      result.body;

    expect(_id).to.be.a("string").that.is.not.empty;
    expect(_entityData).to.be.a("object").that.is.not.null;
    expect(FullName).to.be.a("string").that.is.not.empty;
    expect(CompanyName).to.be.a("string").that.is.not.empty;
    expect(Address).to.be.a("string").that.is.not.empty;
    expect(Zipcode).to.be.a("string").that.is.not.empty;
  });
  it("R) should not read an insurer with a false id", async () => {
    const result = await supertest(app)
      .get(`${apiUrl}/${fakeId}`)
      .set("X-JWT-Token", authToken);

    expect(result.statusCode).to.equal(404);
    expect(result.headers["content-type"]).to.equal(
      "application/json; charset=utf-8"
    );
  });

  it("U) should update a correct insurer with a correct id", async () => {
    const result = await supertest(app)
      .put(`${apiUrl}/${insurerId}`)
      .set("X-JWT-Token", authToken)
      .send(testInsurer);

    expect(result.statusCode).to.equal(200);
    expect(result.headers["content-type"]).to.equal(
      "application/json; charset=utf-8"
    );

    const { _id, _entityData, FullName, CompanyName, Address, Zipcode } =
      result.body;

    expect(_id).to.be.a("string").that.is.not.empty;
    expect(_entityData).to.be.a("object").that.is.not.null;
    expect(FullName).to.be.a("string").that.is.not.empty;
    expect(CompanyName).to.be.a("string").that.is.not.empty;
    expect(Address).to.be.a("string").that.is.not.empty;
    expect(Zipcode).to.be.a("string").that.is.not.empty;
  });
  it("U) should not update an insurer with a false id", async () => {
    const result = await supertest(app)
      .put(`${apiUrl}/${fakeId}`)
      .set("X-JWT-Token", authToken)
      .send(testInsurer);

    expect(result.statusCode).to.equal(404);
    expect(result.headers["content-type"]).to.equal(
      "application/json; charset=utf-8"
    );
  });
  it("U) should not update an insurer with incomplete data", async () => {
    const result = await supertest(app)
      .put(`${apiUrl}/${insurerId}`)
      .set("X-JWT-Token", authToken)
      .send(testInsurerIncomplete);

    expect(result.statusCode).to.equal(422);
    expect(result.headers["content-type"]).to.equal(
      "application/json; charset=utf-8"
    );
  });

  it("D) should delete and return data when a correct insurer with a correct id is deleted", async () => {
    const result = await supertest(app)
      .delete(`${apiUrl}/${insurerId}`)
      .set("X-JWT-Token", authToken);

    expect(result.statusCode).to.equal(200);
    expect(result.headers["content-type"]).to.equal(
      "application/json; charset=utf-8"
    );

    const { _id, _entityData, FullName, CompanyName, Address, Zipcode } =
      result.body;

    expect(_id).to.be.a("string").that.is.not.empty;
    expect(_entityData).to.be.a("object").that.is.not.null;
    expect(FullName).to.be.a("string").that.is.not.empty;
    expect(CompanyName).to.be.a("string").that.is.not.empty;
    expect(Address).to.be.a("string").that.is.not.empty;
    expect(Zipcode).to.be.a("string").that.is.not.empty;
  });
  it("D) should not delete an insurer if it is already deleted", async () => {
    const result = await supertest(app)
      .delete(`${apiUrl}/${insurerId}`)
      .set("X-JWT-Token", authToken);

    expect(result.statusCode).to.equal(404);
    expect(result.headers["content-type"]).to.equal(
      "application/json; charset=utf-8"
    );
  });
  it("D) should not delete an insurer with a fake id", async () => {
    const result = await supertest(app)
      .delete(`${apiUrl}/${fakeId}`)
      .set("X-JWT-Token", authToken);

    expect(result.statusCode).to.equal(404);
    expect(result.headers["content-type"]).to.equal(
      "application/json; charset=utf-8"
    );
  });
});
