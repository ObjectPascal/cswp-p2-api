// tslint:disable:no-unused-expression

import { expect } from "chai";
import app from "../index";
import supertest from "supertest";

const apiUrl: string = "/api/v1/policyholders";
const fakeId: string = "61b0b3359a6830143701e4f8";
const testUser: any = {
  UserName: "user1",
  PasswordHash:
    "a4e4570c263dbfe7e7b01c451f5121f025c7bba885bb3c373ba4a578b4a7dca4",
};
const testPolicyHolder: any = {
  IBAN: "NL40 12345",
  FullName: "Mocha Test Policyholder",
  Initials: "MTP",
  Email: "mocha@test.nl",
  Telephone: "+316 12345678",
  Address: "Mocha Test Address",
  Zipcode: "0987ZY",
  Lat: 123.45,
  Long: 123.45,
};
const testPolicyHolderNonRequired: any = {
  IBAN: "NL40 12345",
  FullName: "Mocha Test Policyholder",
  Initials: "MTP",
  Email: "mocha@test.nl",
  Telephone: "+316 12345678",
  Address: "Mocha Test Address",
  Zipcode: "0987ZY",
};
const testPolicyHolderIncomplete: any = {
  Initials: "MTP",
  Lat: 123.45,
  Long: 123.45,
};

describe("> policyholder tests", () => {
  let authToken: string = "";
  before(async () => {
    const result = await supertest(app).post("/api/auth").send(testUser);
    const { token } = result.body;
    authToken = token;
  });

  let policyHolderId: string = "";
  let policyHolderIdNonRequired: string = "";
  it("C) should create a correct policyholder with a correct id", async () => {
    const result = await supertest(app)
      .post(apiUrl)
      .set("X-JWT-Token", authToken)
      .send(testPolicyHolder);

    expect(result.statusCode).to.equal(201);
    expect(result.headers["content-type"]).to.equal(
      "application/json; charset=utf-8"
    );

    const {
      _id,
      _entityData,
      IBAN,
      FullName,
      Initials,
      Email,
      Telephone,
      Address,
      Zipcode,
      Lat,
      Long,
    } = result.body;

    expect(_id).to.be.a("string").that.is.not.empty;
    policyHolderId = _id;

    expect(_entityData).to.be.a("object").that.is.not.null;
    expect(IBAN).to.be.a("string").that.equals("NL40 12345");
    expect(FullName).to.be.a("string").that.equals("Mocha Test Policyholder");
    expect(Initials).to.be.a("string").that.equals("MTP");
    expect(Email).to.be.a("string").that.equals("mocha@test.nl");
    expect(Telephone).to.be.a("string").that.equals("+316 12345678");
    expect(Address).to.be.a("string").that.equals("Mocha Test Address");
    expect(Zipcode).to.be.a("string").that.equals("0987ZY");
    expect(Lat).to.be.a("number").that.equals(123.45);
    expect(Long).to.be.a("number").that.equals(123.45);
  });
  it("C) should create a correct policyholder with only the required parameters", async () => {
    const result = await supertest(app)
      .post(apiUrl)
      .set("X-JWT-Token", authToken)
      .send(testPolicyHolderNonRequired);

    expect(result.statusCode).to.equal(201);
    expect(result.headers["content-type"]).to.equal(
      "application/json; charset=utf-8"
    );

    const {
      _id,
      _entityData,
      IBAN,
      FullName,
      Initials,
      Email,
      Telephone,
      Address,
      Zipcode,
      Lat,
      Long,
    } = result.body;

    expect(_id).to.be.a("string").that.is.not.empty;
    policyHolderIdNonRequired = _id;

    expect(_entityData).to.be.a("object").that.is.not.null;
    expect(IBAN).to.be.a("string").that.equals("NL40 12345");
    expect(FullName).to.be.a("string").that.equals("Mocha Test Policyholder");
    expect(Initials).to.be.a("string").that.equals("MTP");
    expect(Email).to.be.a("string").that.equals("mocha@test.nl");
    expect(Telephone).to.be.a("string").that.equals("+316 12345678");
    expect(Address).to.be.a("string").that.equals("Mocha Test Address");
    expect(Zipcode).to.be.a("string").that.equals("0987ZY");
    expect(Lat).to.be.a("number").that.equals(0);
    expect(Long).to.be.a("number").that.equals(0);
  });
  it("C) should not create a policyholder with incomplete data", async () => {
    const result = await supertest(app)
      .post(apiUrl)
      .set("X-JWT-Token", authToken)
      .send(testPolicyHolderIncomplete);

    expect(result.statusCode).to.equal(422);
    expect(result.headers["content-type"]).to.equal(
      "application/json; charset=utf-8"
    );
  });

  it("R) should provide a list of policyholders", async () => {
    const result = await supertest(app)
      .get(`${apiUrl}/list`)
      .set("X-JWT-Token", authToken);

    expect(result.statusCode).to.equal(200);
    expect(result.headers["content-type"]).to.equal(
      "application/json; charset=utf-8"
    );

    expect(result.body).to.not.be.null;
    expect(result.body).to.have.length.greaterThan(0);
    expect(result.body).to.be.a("array");
  });

  it("R) should read a correct policyholder", async () => {
    const result = await supertest(app)
      .get(`${apiUrl}/${policyHolderId}`)
      .set("X-JWT-Token", authToken);

    expect(result.statusCode).to.equal(200);
    expect(result.headers["content-type"]).to.equal(
      "application/json; charset=utf-8"
    );

    const {
      _id,
      _entityData,
      IBAN,
      FullName,
      Initials,
      Email,
      Telephone,
      Address,
      Zipcode,
      Lat,
      Long,
    } = result.body;

    expect(_id).to.be.a("string").that.is.not.empty;
    expect(_entityData).to.be.a("object").that.is.not.null;
    expect(IBAN).to.be.a("string").that.is.not.empty;
    expect(FullName).to.be.a("string").that.is.not.empty;
    expect(Initials).to.be.a("string").that.is.not.empty;
    expect(Email).to.be.a("string").that.is.not.empty;
    expect(Telephone).to.be.a("string").that.is.not.empty;
    expect(Address).to.be.a("string").that.is.not.empty;
    expect(Zipcode).to.be.a("string").that.is.not.empty;
    expect(Lat).to.be.a("number").that.is.above(-1);
    expect(Long).to.be.a("number").that.is.above(-1);
  });
  it("R) should not read a policyholder with a false id", async () => {
    const result = await supertest(app)
      .get(`${apiUrl}/${fakeId}`)
      .set("X-JWT-Token", authToken);

    expect(result.statusCode).to.equal(404);
    expect(result.headers["content-type"]).to.equal(
      "application/json; charset=utf-8"
    );
  });

  it("U) should update a correct policyholder with a correct id", async () => {
    const result = await supertest(app)
      .put(`${apiUrl}/${policyHolderId}`)
      .set("X-JWT-Token", authToken)
      .send(testPolicyHolder);

    expect(result.statusCode).to.equal(200);
    expect(result.headers["content-type"]).to.equal(
      "application/json; charset=utf-8"
    );

    const {
      _id,
      _entityData,
      IBAN,
      FullName,
      Initials,
      Email,
      Telephone,
      Address,
      Zipcode,
      Lat,
      Long,
    } = result.body;

    expect(_id).to.be.a("string").that.is.not.empty;
    expect(_entityData).to.be.a("object").that.is.not.null;
    expect(IBAN).to.be.a("string").that.is.not.empty;
    expect(FullName).to.be.a("string").that.is.not.empty;
    expect(Initials).to.be.a("string").that.is.not.empty;
    expect(Email).to.be.a("string").that.is.not.empty;
    expect(Telephone).to.be.a("string").that.is.not.empty;
    expect(Address).to.be.a("string").that.is.not.empty;
    expect(Zipcode).to.be.a("string").that.is.not.empty;
    expect(Lat).to.be.a("number").that.is.above(-1);
    expect(Long).to.be.a("number").that.is.above(-1);
  });
  it("U) should not update a policyholder with a false id", async () => {
    const result = await supertest(app)
      .put(`${apiUrl}/${fakeId}`)
      .set("X-JWT-Token", authToken)
      .send(testPolicyHolder);

    expect(result.statusCode).to.equal(404);
    expect(result.headers["content-type"]).to.equal(
      "application/json; charset=utf-8"
    );
  });
  it("U) should not update a policyholder with incomplete data", async () => {
    const result = await supertest(app)
      .put(`${apiUrl}/${policyHolderId}`)
      .set("X-JWT-Token", authToken)
      .send(testPolicyHolderIncomplete);

    expect(result.statusCode).to.equal(422);
    expect(result.headers["content-type"]).to.equal(
      "application/json; charset=utf-8"
    );
  });

  it("D) should delete a correct policyholder with a correct id", async () => {
    const result = await supertest(app)
      .delete(`${apiUrl}/${policyHolderIdNonRequired}`)
      .set("X-JWT-Token", authToken);

    expect(result.statusCode).to.equal(200);
    expect(result.headers["content-type"]).to.equal(
      "application/json; charset=utf-8"
    );
  });
  it("D) should return data when a correct policyholder with a correct id is deleted", async () => {
    const result = await supertest(app)
      .delete(`${apiUrl}/${policyHolderId}`)
      .set("X-JWT-Token", authToken);

    expect(result.statusCode).to.equal(200);
    expect(result.headers["content-type"]).to.equal(
      "application/json; charset=utf-8"
    );

    const {
      _id,
      _entityData,
      IBAN,
      FullName,
      Initials,
      Email,
      Telephone,
      Address,
      Zipcode,
      Lat,
      Long,
    } = result.body;

    expect(_id).to.be.a("string").that.is.not.empty;
    expect(_entityData).to.be.a("object").that.is.not.null;
    expect(IBAN).to.be.a("string").that.is.not.empty;
    expect(FullName).to.be.a("string").that.is.not.empty;
    expect(Initials).to.be.a("string").that.is.not.empty;
    expect(Email).to.be.a("string").that.is.not.empty;
    expect(Telephone).to.be.a("string").that.is.not.empty;
    expect(Address).to.be.a("string").that.is.not.empty;
    expect(Zipcode).to.be.a("string").that.is.not.empty;
    expect(Lat).to.be.a("number").that.is.above(-1);
    expect(Long).to.be.a("number").that.is.above(-1);
  });
  it("D) should not delete a policyholder if it is already deleted", async () => {
    const result = await supertest(app)
      .delete(`${apiUrl}/${policyHolderId}`)
      .set("X-JWT-Token", authToken);

    expect(result.statusCode).to.equal(404);
    expect(result.headers["content-type"]).to.equal(
      "application/json; charset=utf-8"
    );
  });
  it("D) should not delete a policyholder with a fake id", async () => {
    const result = await supertest(app)
      .delete(`${apiUrl}/${fakeId}`)
      .set("X-JWT-Token", authToken);

    expect(result.statusCode).to.equal(404);
    expect(result.headers["content-type"]).to.equal(
      "application/json; charset=utf-8"
    );
  });
});
