// tslint:disable:no-unused-expression

import { expect } from "chai";
import app from "../index";
import supertest from "supertest";

const testPayload: any = {
  UserName: "user1",
  PasswordHash:
    "a4e4570c263dbfe7e7b01c451f5121f025c7bba885bb3c373ba4a578b4a7dca4",
};
const testIncorrectPayload: any = {
  UserName: "fakeuser",
  PasswordHash:
    "a4e4570c263dbfe7e7b01c451f5121f025c7bba885bb3c373ba4a578b4a7dca4",
};
const testIncompletePayload: any = {
  UserName: "user1",
};

describe("> Authentication tests", () => {
  it("should correctly authenticate", async () => {
    const result = await supertest(app).post("/api/auth").send(testPayload);
    expect(result.statusCode).to.equal(200);
    expect(result.headers["content-type"]).to.equal(
      "application/json; charset=utf-8"
    );
  });

  it("should incorrectly authenticate with false data", async () => {
    const result = await supertest(app)
      .post("/api/auth")
      .send(testIncorrectPayload);
    expect(result.statusCode).to.equal(404);
    expect(result.headers["content-type"]).to.equal(
      "application/json; charset=utf-8"
    );
  });

  it("should incorrectly authenticate with incomplete data", async () => {
    const result = await supertest(app)
      .post("/api/auth")
      .send(testIncompletePayload);
    expect(result.statusCode).to.equal(422);
    expect(result.headers["content-type"]).to.equal(
      "application/json; charset=utf-8"
    );
  });

  let authToken: string = "";
  it("should correctly send a JWT", async () => {
    const result = await supertest(app).post("/api/auth").send(testPayload);
    expect(result.statusCode).to.equal(200);
    expect(result.headers["content-type"]).to.equal(
      "application/json; charset=utf-8"
    );

    const { token, issued, expires } = result.body;
    expect(token).to.be.a("string").that.is.not.empty;
    authToken = token;

    expect(issued).to.be.a("number").that.is.above(0);
    expect(expires).to.be.a("number").that.is.above(0);
    expect(new Date(issued)).to.be.a("Date");
    expect(new Date(expires)).to.be.a("Date");
  });

  it("should correctly return the status for valid token", async () => {
    const result = await supertest(app)
      .get("/api/auth/status")
      .set("X-JWT-Token", authToken);

    expect(result.statusCode).to.equal(200);
    expect(result.headers["content-type"]).to.equal(
      "application/json; charset=utf-8"
    );
    const { status, expires, session } = result.body;

    expect(status).to.be.a("string").to.equal("active");
    expect(new Date(Date.parse(expires))).to.be.a("Date");
    expect(session).to.not.be.a("null");
  });

  it("should correctly return the status for expired token", async () => {
    const expiredToken: string =
      "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJpZCI6MSwidXNlcm5hbWUiOiJVc2VyIDEiLCJkYXRlQ3JlYXRlZCI6OCwiaXNzdWVkIjoxNjM4OTcwMTQwNzg0LCJleHBpcmVzIjoxNjM4OTczNzQwNzg0fQ.FrhlLHuyc2KVgUrDw2HTcbxuiuNYoXGvu1tKcypBm3JFZ6k8_svz_bJH2f7V5Dsk-CUF8tWUoQTJGXeHzDteMQ";
    const result = await supertest(app)
      .get("/api/auth/status")
      .set("X-JWT-Token", expiredToken);

    expect(result.statusCode).to.equal(200);
    expect(result.headers["content-type"]).to.equal(
      "application/json; charset=utf-8"
    );
    const { status, expires, session } = result.body;

    expect(status).to.be.a("string").to.equal("expired");
    expect(new Date(Date.parse(expires))).to.be.a("Date");
    expect(session).to.not.be.a("null");
  });
});
