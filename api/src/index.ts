import express from "express";
import { requireJWTAuth } from "./auth/authMiddleWare";
import V1ApiRouter from "./v1/v1ApiRouter";
import mongoose from "mongoose";
import bodyParser from "body-parser";
import AuthRouter from "./auth/authRouter";

const jsonBodyParser = bodyParser.json();
const app = express();
const port = process.env.PORT || 8080;

// TODO: Store Mongodb + Neo4j conn string & secret key in config + heroku variable (ENVIRONMENT VARIABLES)
// TODO: ChangedById's should reference user ID's in Neo4j db

mongoose.connect(
  "mongodb+srv://admin:avans_admin_2021@cswp-api.4xtic.mongodb.net/CSWP-InsuranceRetail?retryWrites=true&w=majority",
  {},
  (error) => {
    if (error) {
      // tslint:disable-next-line:no-console
      console.log(`error while connecting to database ${error.message}`);
    } else {
      // tslint:disable-next-line:no-console
      console.log("connected to database");
    }
  }
);

app.all("*", function (req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header(
    "Access-Control-Allow-Headers",
    "Access-Control-Allow-Origin, Access-Control-Allow-Headers, Access-Control-Allow-Methods, Access-Control-Request-Method, Access-Control-Request-Origin, Origin, Accept, X-Requested-With, X-JWT-Token, Content-Type, Access-Control-Request-Headers, Pragma, Cache-Control, Expires"
  );
  res.header(
    "Access-Control-Allow-Methods",
    "GET,POST,UPDATE,PUT,PATCH,DELETE,OPTIONS"
  );

  if (req.method == "OPTIONS") {
    res.sendStatus(200);
  } else {
    next();
  }
});

app.use("/api/v1", requireJWTAuth, V1ApiRouter);
app.use("/api/auth", jsonBodyParser, AuthRouter);

app.listen(port, () => {
  // tslint:disable-next-line:no-console
  console.log(`server started at http://localhost:${port}`);
});

export default app;
