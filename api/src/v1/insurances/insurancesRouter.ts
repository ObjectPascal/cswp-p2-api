import { Response, Request, Router } from "express";
import { Insurance } from "./Insurance";
import mongoose from "mongoose";
import { getSessionBearerId } from "../../auth/jwtCore";

const InsuranceRouter = Router();

InsuranceRouter.get("/list", async (req: Request, res: Response) => {
  try {
    const result = await Insurance.find()
      .populate("_insurer")
      .populate("_insuranceType")
      .populate("_insurancePolicy");
    if (result) {
      return res.status(200).send(result);
    } else {
      return res.status(404).send({
        ok: false,
        status: 404,
        message:
          "Failed to retrieve a list of insurer entities. Reason: The operation yielded no results.",
      });
    }
  } catch (_e) {
    return res.status(500).send({
      ok: false,
      status: 500,
      message: `Failed to retrieve a list of insurer entities. Reason: Server error ${_e.message}`,
    });
  }
});
InsuranceRouter.get("/:id", async (req: Request, res: Response) => {
  try {
    if (req.params.id) {
      const result = await Insurance.findById(req.params.id)
        .populate("_insurer")
        .populate("_insuranceType")
        .populate("_insurancePolicy");
      if (result) {
        return res.status(200).send(result);
      } else {
        return res.status(404).send({
          ok: false,
          status: 404,
          message:
            "Failed to retrieve insurance entity. Reason: Entity does not exist.",
        });
      }
    } else {
      return res.status(422).send({
        ok: false,
        status: 422,
        message:
          "Failed to process insurance entity. Reason: ID parameter is empty.",
      });
    }
  } catch (_e) {
    return res.status(500).send({
      ok: false,
      status: 500,
      message: `Failed to retrieve insurance entity. Reason: Server error ${_e.message}`,
    });
  }
});
InsuranceRouter.post("/", async (req: Request, res: Response) => {
  try {
    const {
      _insurer,
      _insuranceType,
      _insurancePolicy,
      PaymentPeriodMonths,
      PrevCollectionDate,
      NextCollectionDate,
      Description,
    } = req.body;
    if (
      _insurer &&
      _insuranceType &&
      _insurancePolicy &&
      PaymentPeriodMonths &&
      PrevCollectionDate &&
      NextCollectionDate &&
      Description
    ) {
      const doc = Insurance.build({
        _entityData: {
          _changedById: getSessionBearerId(req),
          LastChanged: new Date(),
        },
        _insurer: new mongoose.Types.ObjectId(_insurer),
        _insuranceType: new mongoose.Types.ObjectId(_insuranceType),
        _insurancePolicy: new mongoose.Types.ObjectId(_insurancePolicy),
        PaymentPeriodMonths,
        PrevCollectionDate,
        NextCollectionDate,
        Description,
      });
      const result = await doc.save();
      return res.status(201).send(result);
    } else {
      return res.status(422).send({
        ok: false,
        status: 422,
        message:
          "Failed to process insurance entity. Reason: Missing required parameters.",
      });
    }
  } catch (_e) {
    return res.status(500).send({
      ok: false,
      status: 500,
      message: `Failed to process insurance entity. Reason: Server error ${_e.message}`,
    });
  }
});
InsuranceRouter.put("/:id", async (req: Request, res: Response) => {
  try {
    if (req.params.id) {
      const bearerId = getSessionBearerId(req);
      const checkObj: any = await Insurance.findById(req.params.id);
      if (checkObj) {
        const { _changedById } = checkObj._entityData;
        if (bearerId === _changedById) {
          const {
            _insurer,
            _insuranceType,
            _insurancePolicy,
            PaymentPeriodMonths,
            PrevCollectionDate,
            NextCollectionDate,
            Description,
          } = req.body;
          if (
            _insurer &&
            _insuranceType &&
            _insurancePolicy &&
            PaymentPeriodMonths &&
            PrevCollectionDate &&
            NextCollectionDate &&
            Description
          ) {
            const result = await Insurance.findByIdAndUpdate(req.params.id, {
              _entityData: {
                _changedById: bearerId,
                LastChanged: new Date(),
              },
              _insurer: new mongoose.Types.ObjectId(_insurer),
              _insuranceType: new mongoose.Types.ObjectId(_insuranceType),
              _insurancePolicy: new mongoose.Types.ObjectId(_insurancePolicy),
              PaymentPeriodMonths,
              PrevCollectionDate,
              NextCollectionDate,
              Description,
            })
              .populate("_insurer")
              .populate("_insuranceType")
              .populate("_insurancePolicy");
            if (result) {
              return res.status(200).send(result);
            } else {
              return res.status(404).send({
                ok: false,
                status: 404,
                message:
                  "Failed to process insurance entity. Reason: Entity does not exist.",
              });
            }
          } else {
            return res.status(422).send({
              ok: false,
              status: 422,
              message:
                "Failed to process insurance entity. Reason: Missing required parameters.",
            });
          }
        } else {
          return res.status(401).send({
            ok: false,
            status: 401,
            message:
              "Failed to process insurance entity. Reason: Entity change rejected.",
          });
        }
      } else {
        return res.status(404).send({
          ok: false,
          status: 404,
          message:
            "Failed to process insurance entity. Reason: Unable to check entity isolation since the entity does not exist.",
        });
      }
    } else {
      return res.status(422).send({
        ok: false,
        status: 422,
        message:
          "Failed to process insurance entity. Reason: ID parameter is empty.",
      });
    }
  } catch (_e) {
    return res.status(500).send({
      ok: false,
      status: 500,
      message: `Failed to process insurance entity. Reason: Server error ${_e.message}`,
    });
  }
});
InsuranceRouter.delete("/:id", async (req: Request, res: Response) => {
  try {
    if (req.params.id) {
      const bearerId = getSessionBearerId(req);
      const checkObj: any = await Insurance.findById(req.params.id);
      if (checkObj) {
        const { _changedById } = checkObj._entityData;
        if (bearerId === _changedById) {
          const result = await Insurance.findByIdAndRemove(req.params.id)
            .populate("_insurer")
            .populate("_insuranceType")
            .populate("_insurancePolicy");
          if (result) {
            return res.status(200).send(result);
          } else {
            return res.status(404).send({
              ok: false,
              status: 404,
              message:
                "Failed to process insurance entity. Reason: Entity does not exist.",
            });
          }
        } else {
          return res.status(401).send({
            ok: false,
            status: 401,
            message:
              "Failed to process insurance entity. Reason: Entity change rejected.",
          });
        }
      } else {
        return res.status(404).send({
          ok: false,
          status: 404,
          message:
            "Failed to process insurance entity. Reason: Unable to check entity isolation since the entity does not exist.",
        });
      }
    } else {
      return res.status(422).send({
        ok: false,
        status: 422,
        message:
          "Failed to process insurance entity. Reason: ID parameter is empty.",
      });
    }
  } catch (_e) {
    return res.status(500).send({
      ok: false,
      status: 500,
      message: `Failed to process insurance entity. Reason: Server error ${_e.message}`,
    });
  }
});
export default InsuranceRouter;
