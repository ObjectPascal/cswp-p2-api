// tslint:disable:no-unused-expression

import { expect } from "chai";
import app from "../index";
import supertest from "supertest";

const apiUrl: string = "/api/v1/users";
const fakeId: number = 9999999;
const testUser: any = {
  UserName: "user1",
  PasswordHash:
    "a4e4570c263dbfe7e7b01c451f5121f025c7bba885bb3c373ba4a578b4a7dca4",
};

const testUserNode1: any = {
  UserName: "Mocha Test User 1",
  PasswordHash: "MochaTestHash1",
  Data: {
    _insurerId: "MochaTestId1",
    CompanyName: "Mocha Test Company",
    ZipCode: "0987ZY",
  },
};
const testUserNode1Updated: any = {
  UserName: "Mocha Test User 1 Updated",
  PasswordHash: "MochaTestHash1Updated",
  Data: {
    _insurerId: "MochaTestId1Updated",
    CompanyName: "Mocha Test Company",
    ZipCode: "0987ZY",
  },
};
const testUserNode2: any = {
  UserName: "Mocha Test User 2",
  PasswordHash: "MochaTestHash2",
  Data: {
    _insurerId: "MochaTestId2",
    CompanyName: "Mocha Test Company",
    ZipCode: "0987ZY",
  },
};
const testUserNodeIncomplete: any = {
  PasswordHash: "MochaTestHash",
  Data: {
    _insurerId: "MochaTestId",
  },
};
const testUserNodeOnlyRequired: any = {
  UserName: "Mocha Test User",
  Data: {
    _insurerId: "MochaTestId",
    CompanyName: "Mocha Test Company",
    ZipCode: "0987ZY",
  },
};

describe("> user tests", () => {
  let authToken: string = "";
  before(async () => {
    const result = await supertest(app).post("/api/auth").send(testUser);
    const { token } = result.body;
    authToken = token;
  });

  let userId1: number = -1;
  let userId2: number = -1;
  it("C) should create a correct first user", async () => {
    const result = await supertest(app)
      .post(apiUrl)
      .set("X-JWT-Token", authToken)
      .send(testUserNode1);

    expect(result.statusCode).to.equal(201);
    expect(result.headers["content-type"]).to.equal(
      "application/json; charset=utf-8"
    );
    const { id, UserName, Data } = result.body;
    const { _insurerId, CompanyName, ZipCode } = Data;

    expect(id).to.be.a("number").that.is.above(-1);
    userId1 = id;

    expect(UserName).to.be.a("string").that.equals("Mocha Test User 1");
    expect(CompanyName).to.be.a("string").that.equals("Mocha Test Company");
    expect(Data).to.be.a("object").that.is.not.null;
    expect(_insurerId).to.be.a("string").that.equals("MochaTestId1");
    expect(ZipCode).to.be.a("string").that.equals("0987ZY");
  });
  it("C) should create a correct second user", async () => {
    const result = await supertest(app)
      .post(apiUrl)
      .set("X-JWT-Token", authToken)
      .send(testUserNode2);

    expect(result.statusCode).to.equal(201);
    expect(result.headers["content-type"]).to.equal(
      "application/json; charset=utf-8"
    );
    const { id, UserName, Data } = result.body;
    const { _insurerId, CompanyName, ZipCode } = Data;

    expect(id).to.be.a("number").that.is.above(-1);
    userId2 = id;

    expect(UserName).to.be.a("string").that.equals("Mocha Test User 2");
    expect(CompanyName).to.be.a("string").that.equals("Mocha Test Company");
    expect(Data).to.be.a("object").that.is.not.null;
    expect(_insurerId).to.be.a("string").that.equals("MochaTestId2");
    expect(ZipCode).to.be.a("string").that.equals("0987ZY");
  });
  it("C) should not create a user with incomplete data", async () => {
    const result = await supertest(app)
      .post(apiUrl)
      .set("X-JWT-Token", authToken)
      .send(testUserNodeIncomplete);

    expect(result.statusCode).to.equal(422);
    expect(result.headers["content-type"]).to.equal(
      "application/json; charset=utf-8"
    );
  });

  it("R) should read a correct user", async () => {
    const result = await supertest(app)
      .get(`${apiUrl}/${userId1}`)
      .set("X-JWT-Token", authToken);

    expect(result.statusCode).to.equal(200);
    expect(result.headers["content-type"]).to.equal(
      "application/json; charset=utf-8"
    );

    const { id, UserName, Data } = result.body;
    const { _insurerId, CompanyName, ZipCode } = Data;

    expect(id).to.be.a("number").that.is.above(-1);
    expect(UserName).to.be.a("string").that.is.not.empty;
    expect(CompanyName).to.be.a("string").that.is.not.empty;
    expect(Data).to.be.a("object").that.is.not.null;
    expect(_insurerId).to.be.a("string").that.is.not.empty;
    expect(ZipCode).to.be.a("string").that.is.not.empty;
  });
  it("R) should read the correct WORKSWITH relationship from a user", async () => {
    const result = await supertest(app)
      .get(`${apiUrl}/${userId1}/workswith`)
      .set("X-JWT-Token", authToken);

    expect(result.statusCode).to.equal(200);
    expect(result.headers["content-type"]).to.equal(
      "application/json; charset=utf-8"
    );
    expect(result.body).to.be.a("array");

    const { id, UserName, Data } = result.body[0];
    const { _insurerId, CompanyName, ZipCode } = Data;

    expect(id).to.be.a("number").that.equals(userId2);
    expect(UserName).to.be.a("string").that.equals("Mocha Test User 2");
    expect(CompanyName).to.be.a("string").that.equals("Mocha Test Company");
    expect(Data).to.be.a("object").that.is.not.null;
    expect(_insurerId).to.be.a("string").that.equals("MochaTestId2");
    expect(ZipCode).to.be.a("string").that.equals("0987ZY");
  });
  it("R) should read the correct LIVESCLOSETO relationship from a user", async () => {
    const result = await supertest(app)
      .get(`${apiUrl}/${userId1}/livescloseto`)
      .set("X-JWT-Token", authToken);

    expect(result.statusCode).to.equal(200);
    expect(result.headers["content-type"]).to.equal(
      "application/json; charset=utf-8"
    );
    expect(result.body).to.be.a("array");

    const { id, UserName, Data } = result.body[0];
    const { _insurerId, CompanyName, ZipCode } = Data;

    expect(id).to.be.a("number").that.equals(userId2);
    expect(UserName).to.be.a("string").that.equals("Mocha Test User 2");
    expect(CompanyName).to.be.a("string").that.equals("Mocha Test Company");
    expect(Data).to.be.a("object").that.is.not.null;
    expect(_insurerId).to.be.a("string").that.equals("MochaTestId2");
    expect(ZipCode).to.be.a("string").that.equals("0987ZY");
  });
  it("R) should not read a user with a false id", async () => {
    const result = await supertest(app)
      .get(`${apiUrl}/${fakeId}`)
      .set("X-JWT-Token", authToken);

    expect(result.statusCode).to.equal(404);
    expect(result.headers["content-type"]).to.equal(
      "application/json; charset=utf-8"
    );
  });

  it("U) should update a correct user with a correct id", async () => {
    const result = await supertest(app)
      .put(`${apiUrl}/${userId1}`)
      .set("X-JWT-Token", authToken)
      .send(testUserNode1Updated);

    expect(result.statusCode).to.equal(200);
    expect(result.headers["content-type"]).to.equal(
      "application/json; charset=utf-8"
    );

    const { id, UserName, Data } = result.body;
    const { _insurerId, CompanyName, ZipCode } = Data;

    expect(id).to.be.a("number").that.is.above(-1);
    expect(UserName).to.be.a("string").that.is.not.empty;
    expect(CompanyName).to.be.a("string").that.is.not.empty;
    expect(Data).to.be.a("object").that.is.not.null;
    expect(_insurerId).to.be.a("string").that.is.not.empty;
    expect(ZipCode).to.be.a("string").that.is.not.empty;
  });
  it("U) should not update a user with a false id", async () => {
    const result = await supertest(app)
      .put(`${apiUrl}/${fakeId}`)
      .set("X-JWT-Token", authToken)
      .send(testUserNode1Updated);

    expect(result.statusCode).to.equal(404);
    expect(result.headers["content-type"]).to.equal(
      "application/json; charset=utf-8"
    );
  });
  it("U) should not update a user with incomplete data", async () => {
    const result = await supertest(app)
      .put(`${apiUrl}/${userId1}`)
      .set("X-JWT-Token", authToken)
      .send(testUserNodeIncomplete);

    expect(result.statusCode).to.equal(422);
    expect(result.headers["content-type"]).to.equal(
      "application/json; charset=utf-8"
    );
  });

  it("D) should delete a user with a correct id", async () => {
    const result = await supertest(app)
      .delete(`${apiUrl}/${userId1}`)
      .set("X-JWT-Token", authToken);

    expect(result.statusCode).to.equal(200);
    expect(result.headers["content-type"]).to.equal(
      "application/json; charset=utf-8"
    );
  });
  it("D) should return data when a correct user with a correct id is deleted", async () => {
    const result = await supertest(app)
      .delete(`${apiUrl}/${userId2}`)
      .set("X-JWT-Token", authToken);

    expect(result.statusCode).to.equal(200);
    expect(result.headers["content-type"]).to.equal(
      "application/json; charset=utf-8"
    );

    const { id } = result.body;

    expect(id).to.be.a("number").that.is.above(-1);
    expect(id).to.equal(userId2);
  });
  it("D) should not delete an insurer if it is already deleted", async () => {
    const result = await supertest(app)
      .delete(`${apiUrl}/${userId2}`)
      .set("X-JWT-Token", authToken);

    expect(result.statusCode).to.equal(404);
    expect(result.headers["content-type"]).to.equal(
      "application/json; charset=utf-8"
    );
  });
  it("D) should not delete an insurer with a fake id", async () => {
    const result = await supertest(app)
      .delete(`${apiUrl}/${fakeId}`)
      .set("X-JWT-Token", authToken);

    expect(result.statusCode).to.equal(404);
    expect(result.headers["content-type"]).to.equal(
      "application/json; charset=utf-8"
    );
  });
});
