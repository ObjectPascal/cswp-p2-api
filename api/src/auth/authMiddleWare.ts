import { Request, Response, NextFunction } from "express";
import { DecodeResult } from "./DecodeResult";
import {
  checkExpirationStatus,
  decodeSession,
  ExpirationStatus,
} from "./jwtCore";
import { Session } from "./Session";

export function requireJWTAuth(
  request: Request,
  response: Response,
  next: NextFunction
) {
  const unauthorized = (message: string) =>
    response.status(401).json({
      ok: false,
      status: 401,
      message,
    });

  const requestHeader = "X-JWT-Token";
  const header = request.header(requestHeader);

  if (!header) {
    unauthorized(`Required ${requestHeader} header not found.`);
    return;
  }

  const decodedSession: DecodeResult = decodeSession(
    "726c7017-b252-4f44-af9b-71ceb034b69c",
    header
  );

  if (
    decodedSession.type === "integrity-error" ||
    decodedSession.type === "invalid-token"
  ) {
    unauthorized(
      `Failed to decode or validate authorization token. Reason: ${decodedSession.type}.`
    );
    return;
  }

  const expiration: ExpirationStatus = checkExpirationStatus(
    decodedSession.session
  );

  if (expiration === "expired") {
    unauthorized(
      `Authorization token has expired. Please create a new authorization token.`
    );
    return;
  }
  const session: Session = decodedSession.session;
  response.locals = {
    ...response.locals,
    session,
  };
  next();
}
