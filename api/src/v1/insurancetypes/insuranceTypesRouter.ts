import { Response, Request, Router } from "express";
import { InsuranceType } from "./InsuranceType";

const InsuranceTypesRouter = Router();

InsuranceTypesRouter.get("/list", async (req: Request, res: Response) => {
  try {
    const result = await InsuranceType.find();
    if (result) {
      return res.status(200).send(result);
    } else {
      return res.status(404).send({
        ok: false,
        status: 404,
        message:
          "Failed to retrieve a list of insurancetype entities. Reason: The operation yielded no results.",
      });
    }
  } catch (_e) {
    return res.status(500).send({
      ok: false,
      status: 500,
      message: `Failed to retrieve a list of insurancetype entities. Reason: Server error ${_e.message}`,
    });
  }
});
InsuranceTypesRouter.get("/:id", async (req: Request, res: Response) => {
  try {
    if (req.params.id) {
      const result = await InsuranceType.findById(req.params.id);
      if (result) {
        return res.status(200).send(result);
      } else {
        return res.status(404).send({
          ok: false,
          status: 404,
          message:
            "Failed to retrieve insurancetype entity. Reason: Entity does not exist.",
        });
      }
    } else {
      return res.status(422).send({
        ok: false,
        status: 422,
        message:
          "Failed to process insurancetype entity. Reason: ID parameter is empty.",
      });
    }
  } catch (_e) {
    return res.status(500).send({
      ok: false,
      status: 500,
      message: `Failed to retrieve insurancetype entity. Reason: Server error ${_e.message}`,
    });
  }
});
InsuranceTypesRouter.post("/", async (req: Request, res: Response) => {
  try {
    const { TypeName, PaymentPeriodMonths } = req.body;
    if (TypeName && PaymentPeriodMonths) {
      const doc = InsuranceType.build({
        _entityData: {
          _changedById: 1,
          LastChanged: new Date(),
        },
        TypeName,
        PaymentPeriodMonths,
      });
      const result = await doc.save();
      return res.status(201).send(result);
    } else {
      return res.status(422).send({
        ok: false,
        status: 422,
        message:
          "Failed to process insurancetype entity. Reason: Missing required parameters.",
      });
    }
  } catch (_e) {
    return res.status(500).send({
      ok: false,
      status: 500,
      message: `Failed to process insurancetype entity. Reason: Server error ${_e.message}`,
    });
  }
});
InsuranceTypesRouter.put("/:id", async (req: Request, res: Response) => {
  try {
    if (req.params.id) {
      const { TypeName, PaymentPeriodMonths } = req.body;
      if (TypeName && PaymentPeriodMonths) {
        const result = await InsuranceType.findByIdAndUpdate(req.params.id, {
          _entityData: {
            _changedById: 1,
            LastChanged: new Date(),
          },
          TypeName,
          PaymentPeriodMonths,
        });
        if (result) {
          return res.status(200).send(result);
        } else {
          return res.status(404).send({
            ok: false,
            status: 404,
            message:
              "Failed to process insurancetype entity. Reason: Entity does not exist.",
          });
        }
      } else {
        return res.status(422).send({
          ok: false,
          status: 422,
          message:
            "Failed to process insurancetype entity. Reason: Missing required parameters.",
        });
      }
    } else {
      return res.status(422).send({
        ok: false,
        status: 422,
        message:
          "Failed to process insurancetype entity. Reason: ID parameter is empty.",
      });
    }
  } catch (_e) {
    return res.status(500).send({
      ok: false,
      status: 500,
      message: `Failed to process insurancetype entity. Reason: Server error ${_e.message}`,
    });
  }
});
InsuranceTypesRouter.delete("/:id", async (req: Request, res: Response) => {
  try {
    if (req.params.id) {
      const result = await InsuranceType.findByIdAndRemove(req.params.id);
      if (result) {
        return res.status(200).send(result);
      } else {
        return res.status(404).send({
          ok: false,
          status: 404,
          message:
            "Failed to process insurancetype entity. Reason: Entity does not exist.",
        });
      }
    } else {
      return res.status(422).send({
        ok: false,
        status: 422,
        message:
          "Failed to process insurancetype entity. Reason: ID parameter is empty.",
      });
    }
  } catch (_e) {
    return res.status(500).send({
      ok: false,
      status: 500,
      message: `Failed to process insurancetype entity. Reason: Server error ${_e.message}`,
    });
  }
});
export default InsuranceTypesRouter;
