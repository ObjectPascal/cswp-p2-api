import mongoose, { Decimal128, Mongoose, SchemaTypes } from "mongoose";

interface IPolicyHolder {
  _entityData: object;
  IBAN: string;
  FullName: string;
  Initials: string;
  Email: string;
  Telephone: string;
  Address: string;
  Zipcode: string;
  Lat: number;
  Long: number;
}

interface PolicyHolderModelInterface extends mongoose.Model<PolicyHolderDoc> {
  build(data: IPolicyHolder): PolicyHolderDoc;
}

interface PolicyHolderDoc extends mongoose.Document {
  _entityData: object;
  IBAN: string;
  FullName: string;
  Initials: string;
  Email: string;
  Telephone: string;
  Address: string;
  Zipcode: string;
  Lat: number;
  Long: number;
}

const schema = new mongoose.Schema(
  {
    _entityData: {
      _changedById: Number,
      LastChanged: {
        type: Date,
        default: new Date(Date.now()),
      },
    },
    IBAN: {
      type: String,
      required: true,
    },
    FullName: {
      type: String,
      required: true,
    },
    Initials: {
      type: String,
      required: true,
    },
    Email: {
      type: String,
      required: true,
    },
    Telephone: {
      type: String,
      required: true,
    },
    Address: {
      type: String,
      required: true,
    },
    Zipcode: {
      type: String,
      required: true,
    },
    Lat: {
      type: Number,
      required: false,
      default: 0,
    },
    Long: {
      type: Number,
      required: false,
      default: 0,
    },
  },
  {
    versionKey: false,
  }
);

schema.statics.build = (data: IPolicyHolder) => {
  return new PolicyHolder(data);
};

const PolicyHolder = mongoose.model<
  PolicyHolderDoc,
  PolicyHolderModelInterface
>("PolicyHolder", schema);
export { PolicyHolder };
