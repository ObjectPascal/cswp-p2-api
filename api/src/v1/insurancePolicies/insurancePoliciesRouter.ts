import { Response, Request, Router } from "express";
import { InsurancePolicy } from "./InsurancePolicy";
import mongoose from "mongoose";

const InsurancePoliciesRouter = Router();

InsurancePoliciesRouter.get("/list", async (req: Request, res: Response) => {
  try {
    const result = await InsurancePolicy.find().populate("_policyHolder");
    if (result) {
      return res.status(200).send(result);
    } else {
      return res.status(404).send({
        ok: false,
        status: 404,
        message:
          "Failed to retrieve a list of insurancepolicy entities. Reason: The operation yielded no results.",
      });
    }
  } catch (_e) {
    return res.status(500).send({
      ok: false,
      status: 500,
      message: `Failed to retrieve a list of insurancepolicy entities. Reason: Server error ${_e.message}`,
    });
  }
});
InsurancePoliciesRouter.get("/:id", async (req: Request, res: Response) => {
  try {
    if (req.params.id) {
      const result = await InsurancePolicy.findById(req.params.id).populate(
        "_policyHolder"
      );
      if (result) {
        return res.status(200).send(result);
      } else {
        return res.status(404).send({
          ok: false,
          status: 404,
          message:
            "Failed to retrieve insurancepolicy entity. Reason: Entity does not exist.",
        });
      }
    } else {
      return res.status(422).send({
        ok: false,
        status: 422,
        message:
          "Failed to process insurancepolicy entity. Reason: ID parameter is empty.",
      });
    }
  } catch (_e) {
    return res.status(500).send({
      ok: false,
      status: 500,
      message: `Failed to retrieve insurancepolicy entity. Reason: Server error ${_e.message}`,
    });
  }
});
InsurancePoliciesRouter.post("/", async (req: Request, res: Response) => {
  try {
    const { _policyHolder, PolicyNumber, Premium, DiscountPercentage } =
      req.body;
    if (_policyHolder && PolicyNumber && Premium) {
      const doc = InsurancePolicy.build({
        _entityData: {
          _changedById: 1,
          LastChanged: new Date(),
        },
        _policyHolder: new mongoose.Types.ObjectId(_policyHolder),
        PolicyNumber,
        Premium,
        DiscountPercentage,
      });
      const result = await doc.save();
      return res.status(201).send(result);
    } else {
      return res.status(422).send({
        ok: false,
        status: 422,
        message:
          "Failed to process insurancepolicy entity. Reason: Missing required parameters.",
      });
    }
  } catch (_e) {
    return res.status(500).send({
      ok: false,
      status: 500,
      message: `Failed to process insurancepolicy entity. Reason: Server error ${_e.message}`,
    });
  }
});
InsurancePoliciesRouter.put("/:id", async (req: Request, res: Response) => {
  try {
    if (req.params.id) {
      const { _policyHolder, PolicyNumber, Premium, DiscountPercentage } =
        req.body;
      if (_policyHolder && PolicyNumber && Premium) {
        const result = await InsurancePolicy.findByIdAndUpdate(req.params.id, {
          _entityData: {
            _changedById: 1,
            LastChanged: new Date(),
          },
          _policyHolder: new mongoose.Types.ObjectId(_policyHolder),
          PolicyNumber,
          Premium,
          DiscountPercentage,
        }).populate("_policyHolder");
        if (result) {
          return res.status(200).send(result);
        } else {
          return res.status(404).send({
            ok: false,
            status: 404,
            message:
              "Failed to process insurancepolicy entity. Reason: Entity does not exist.",
          });
        }
      } else {
        return res.status(422).send({
          ok: false,
          status: 422,
          message:
            "Failed to process insurancepolicy entity. Reason: Missing required parameters.",
        });
      }
    } else {
      return res.status(422).send({
        ok: false,
        status: 422,
        message:
          "Failed to process insurancepolicy entity. Reason: ID parameter is empty.",
      });
    }
  } catch (_e) {
    return res.status(500).send({
      ok: false,
      status: 500,
      message: `Failed to process insurancepolicy entity. Reason: Server error ${_e.message}`,
    });
  }
});
InsurancePoliciesRouter.delete("/:id", async (req: Request, res: Response) => {
  try {
    if (req.params.id) {
      const result = await InsurancePolicy.findByIdAndRemove(
        req.params.id
      ).populate("_policyHolder");
      if (result) {
        return res.status(200).send(result);
      } else {
        return res.status(404).send({
          ok: false,
          status: 404,
          message:
            "Failed to process insurancepolicy entity. Reason: Entity does not exist.",
        });
      }
    } else {
      return res.status(422).send({
        ok: false,
        status: 422,
        message:
          "Failed to process insurancepolicy entity. Reason: ID parameter is empty.",
      });
    }
  } catch (_e) {
    return res.status(500).send({
      ok: false,
      status: 500,
      message: `Failed to process insurancepolicy entity. Reason: Server error ${_e.message}`,
    });
  }
});
export default InsurancePoliciesRouter;
