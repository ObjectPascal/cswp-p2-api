export interface Session {
  id: number;
  dateCreated: number;
  issued: number;
  expires: number;
}
