import { Router } from "express";
import insurerRouter from "./insurers/insurerRouter";
import bodyParser from "body-parser";
import policyHoldersRouter from "./policyholders/policyHoldersRouter";
import InsurancePoliciesRouter from "./insurancePolicies/insurancePoliciesRouter";
import InsuranceTypesRouter from "./insurancetypes/insuranceTypesRouter";
import InsuranceRouter from "./insurances/insurancesRouter";
import usersRouter from "./users/usersRouter";

const jsonBodyParser = bodyParser.json();
const V1ApiRouter = Router();

V1ApiRouter.use("/users", jsonBodyParser, usersRouter);
V1ApiRouter.use("/policyholders", jsonBodyParser, policyHoldersRouter);
V1ApiRouter.use("/insurers", jsonBodyParser, insurerRouter);
V1ApiRouter.use("/insurances", jsonBodyParser, InsuranceRouter);
V1ApiRouter.use("/insurancepolicies", jsonBodyParser, InsurancePoliciesRouter);
V1ApiRouter.use("/insurancetypes", jsonBodyParser, InsuranceTypesRouter);

export default V1ApiRouter;
