import { Response, Request, Router } from "express";
import { Insurer } from "./Insurer";
import { getSessionBearerId } from "../../auth/jwtCore";

const insurerRouter = Router();

insurerRouter.get("/list", async (req: Request, res: Response) => {
  try {
    const result = await Insurer.find();
    if (result) {
      return res.status(200).send(result);
    } else {
      return res.status(404).send({
        ok: false,
        status: 404,
        message:
          "Failed to retrieve a list of insurer entities. Reason: The operation yielded no results.",
      });
    }
  } catch (_e) {
    return res.status(500).send({
      ok: false,
      status: 500,
      message: `Failed to retrieve a list of insurer entities. Reason: Server error ${_e.message}`,
    });
  }
});
insurerRouter.get("/:id", async (req: Request, res: Response) => {
  try {
    if (req.params.id) {
      const result = await Insurer.findById(req.params.id);
      if (result) {
        return res.status(200).send(result);
      } else {
        return res.status(404).send({
          ok: false,
          status: 404,
          message:
            "Failed to retrieve insurer entity. Reason: Entity does not exist.",
        });
      }
    } else {
      return res.status(422).send({
        ok: false,
        status: 422,
        message:
          "Failed to process insurer entity. Reason: ID parameter is empty.",
      });
    }
  } catch (_e) {
    return res.status(500).send({
      ok: false,
      status: 500,
      message: `Failed to retrieve insurer entity. Reason: Server error ${_e.message}`,
    });
  }
});
insurerRouter.post("/", async (req: Request, res: Response) => {
  try {
    const { FullName, CompanyName, Address, Zipcode } = req.body;
    if (FullName && CompanyName && Address && Zipcode) {
      const doc = Insurer.build({
        _entityData: {
          _changedById: getSessionBearerId(req),
          LastChanged: new Date(),
        },
        FullName,
        CompanyName,
        Address,
        Zipcode,
      });
      const result = await doc.save();
      return res.status(201).send(result);
    } else {
      return res.status(422).send({
        ok: false,
        status: 422,
        message:
          "Failed to process insurer entity. Reason: Missing required parameters.",
      });
    }
  } catch (_e) {
    return res.status(500).send({
      ok: false,
      status: 500,
      message: `Failed to process insurer entity. Reason: Server error ${_e.message}`,
    });
  }
});
insurerRouter.put("/:id", async (req: Request, res: Response) => {
  try {
    if (req.params.id) {
      const bearerId = getSessionBearerId(req);
      const checkObj: any = await Insurer.findById(req.params.id);
      if (checkObj) {
        const { _changedById } = checkObj._entityData;
        if (bearerId === _changedById) {
          const { FullName, CompanyName, Address, Zipcode } = req.body;
          if (FullName && CompanyName && Address && Zipcode) {
            const result = await Insurer.findByIdAndUpdate(req.params.id, {
              _entityData: {
                _changedById: bearerId,
                LastChanged: new Date(),
              },
              FullName,
              CompanyName,
              Address,
              Zipcode,
            });
            if (result) {
              return res.status(200).send(result);
            } else {
              return res.status(404).send({
                ok: false,
                status: 404,
                message:
                  "Failed to process insurer entity. Reason: Entity does not exist.",
              });
            }
          } else {
            return res.status(422).send({
              ok: false,
              status: 422,
              message:
                "Failed to process insurer entity. Reason: Missing required parameters.",
            });
          }
        } else {
          return res.status(401).send({
            ok: false,
            status: 401,
            message:
              "Failed to process insurer entity. Reason: Entity change rejected.",
          });
        }
      } else {
        return res.status(404).send({
          ok: false,
          status: 404,
          message:
            "Failed to process insurer entity. Reason: Unable to check entity isolation since the entity does not exist.",
        });
      }
    } else {
      return res.status(422).send({
        ok: false,
        status: 422,
        message:
          "Failed to process insurer entity. Reason: ID parameter is empty.",
      });
    }
  } catch (_e) {
    return res.status(500).send({
      ok: false,
      status: 500,
      message: `Failed to process insurer entity. Reason: Server error ${_e.message}`,
    });
  }
});
insurerRouter.delete("/:id", async (req: Request, res: Response) => {
  try {
    const bearerId = getSessionBearerId(req);
    const checkObj: any = await Insurer.findById(req.params.id);
    if (checkObj) {
      const { _changedById } = checkObj._entityData;
      if (bearerId === _changedById) {
        if (req.params.id) {
          const result = await Insurer.findByIdAndRemove(req.params.id);
          if (result) {
            return res.status(200).send(result);
          } else {
            return res.status(404).send({
              ok: false,
              status: 404,
              message:
                "Failed to process insurer entity. Reason: Entity does not exist.",
            });
          }
        } else {
          return res.status(422).send({
            ok: false,
            status: 422,
            message:
              "Failed to process insurer entity. Reason: ID parameter is empty.",
          });
        }
      } else {
        return res.status(401).send({
          ok: false,
          status: 401,
          message:
            "Failed to process insurer entity. Reason: Entity change rejected.",
        });
      }
    } else {
      return res.status(404).send({
        ok: false,
        status: 404,
        message:
          "Failed to process insurer entity. Reason: Unable to check entity isolation since the entity does not exist.",
      });
    }
  } catch (_e) {
    return res.status(500).send({
      ok: false,
      status: 500,
      message: `Failed to process insurer entity. Reason: Server error ${_e.message}`,
    });
  }
});
export default insurerRouter;
