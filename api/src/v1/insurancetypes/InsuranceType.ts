import mongoose from "mongoose";

interface IInsuranceType {
  _entityData: object;
  TypeName: string;
  PaymentPeriodMonths: number;
}

interface InsuranceTypeModelInterface extends mongoose.Model<InsuranceTypeDoc> {
  build(data: IInsuranceType): InsuranceTypeDoc;
}

interface InsuranceTypeDoc extends mongoose.Document {
  _entityData: object;
  TypeName: string;
  PaymentPeriodMonths: number;
}

const schema = new mongoose.Schema(
  {
    _entityData: {
      _changedById: Number,
      LastChanged: {
        type: Date,
        default: new Date(Date.now()),
      },
    },
    TypeName: {
      type: String,
      required: true,
    },
    PaymentPeriodMonths: {
      type: Number,
      required: true,
    },
  },
  {
    versionKey: false,
  }
);

schema.statics.build = (data: IInsuranceType) => {
  return new InsuranceType(data);
};

const InsuranceType = mongoose.model<
  InsuranceTypeDoc,
  InsuranceTypeModelInterface
>("InsuranceType", schema);
export { InsuranceType };
