import mongoose, { Mongoose, ObjectId, SchemaTypes } from "mongoose";

interface IInsurancePolicy {
  _entityData: object;
  _policyHolder: mongoose.Types.ObjectId;
  PolicyNumber: number;
  Premium: number;
  DiscountPercentage: number;
}

interface InsurancePolicyModelInterface
  extends mongoose.Model<InsurancePolicyDoc> {
  build(data: IInsurancePolicy): InsurancePolicyDoc;
}

interface InsurancePolicyDoc extends mongoose.Document {
  _entityData: object;
  _policyHolder: mongoose.Types.ObjectId;
  PolicyNumber: number;
  Premium: number;
  DiscountPercentage: number;
}

const schema = new mongoose.Schema(
  {
    _entityData: {
      _changedById: Number,
      LastChanged: {
        type: Date,
        default: new Date(Date.now()),
      },
    },
    _policyHolder: {
      type: mongoose.Types.ObjectId,
      ref: "PolicyHolder",
      required: true,
    },
    PolicyNumber: {
      type: Number,
      required: true,
    },
    Premium: {
      type: Number,
      required: true,
    },
    DiscountPercentage: {
      type: Number,
      required: false,
      default: 0,
    },
  },
  {
    versionKey: false,
  }
);

schema.statics.build = (data: IInsurancePolicy) => {
  return new InsurancePolicy(data);
};

const InsurancePolicy = mongoose.model<
  InsurancePolicyDoc,
  InsurancePolicyModelInterface
>("InsurancePolicy", schema);
export { InsurancePolicy };
