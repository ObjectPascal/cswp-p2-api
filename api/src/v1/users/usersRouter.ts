// tslint:disable:prefer-for-of

import { Response, Request, Router } from "express";
import moment from "moment";
import Neode from "neode";

const graphInstance = new Neode(
  "neo4j+s://76211851.databases.neo4j.io",
  "neo4j",
  "0cS9PzdJrTjUNFuvyr3nsmRiBjR6Oh22Tt_9SQEeA_E"
);

// neo4j+s://35c57753.databases.neo4j.io
// JFMgp_VKX2b6szt70VwMKaz6I0FmfewPYVueMxI_qyQ

graphInstance.model("User", {
  id: {
    primary: true,
    type: "uuid",
    required: true,
  },
  UserName: {
    type: "string",
    required: true,
  },
  Initials: {
    type: "string",
    required: true,
  },
  DateOfBirth: {
    type: "datetime",
    required: true,
  },
  Gender: {
    type: "string",
    required: true,
  },
  PasswordHash: {
    type: "string",
    required: true,
  },
  LoginDate: {
    type: "datetime",
    required: true,
  },
});

graphInstance.model("Data", {
  id: {
    primary: true,
    type: "uuid",
    required: true,
  },
  _insurerId: {
    type: "string",
    required: true,
    unique: true,
  },
  CompanyName: {
    type: "string",
    required: true,
  },
  ZipCode: {
    type: "string",
    required: true,
  },
});

const usersRouter = Router();

usersRouter.get("/list", async (req: Request, res: Response) => {
  try {
    const result = await graphInstance.cypher(
      `MATCH (d:Data)<-[:HASDATA]-(u) RETURN u, d`,
      {}
    );
    if (result.records.length > 0) {
      const users: any[] = [];
      for (let i = 0; i < result.records.length; i++) {
        const element: any = result.records[i];
        users.push({
          id: element._fields[0].identity.low,
          UserName: element._fields[0].properties.UserName,
          Initials: element._fields[0].properties.Initials,
          Gender: element._fields[0].properties.Gender,
          DateOfBirth: element._fields[0].properties.DateOfBirth,
          Data: {
            _insurerId: element._fields[1].properties._insurerId,
            CompanyName: element._fields[1].properties.CompanyName,
            ZipCode: element._fields[1].properties.ZipCode,
          },
        });
      }
      return res.status(200).send(users);
    } else {
      return res.status(404).send({
        ok: false,
        status: 404,
        message:
          "Failed to retrieve user entities. Reason: No User entities exist.",
      });
    }
  } catch (_e) {
    return res.status(500).send({
      ok: false,
      status: 500,
      message: `Failed to retrieve user entities. Reason: Server error ${_e.message}`,
    });
  }
});
usersRouter.get("/:id", async (req: Request, res: Response) => {
  try {
    if (req.params.id) {
      const result = await graphInstance.cypher(
        `MATCH (d:Data)<-[:HASDATA]-(u) WHERE id(u) = ${req.params.id} RETURN u, d`,
        {}
      );
      if (result.records.length > 0) {
        const element: any = result.records[0];
        return res.status(200).send({
          id: element._fields[0].identity.low,
          UserName: element._fields[0].properties.UserName,
          Initials: element._fields[0].properties.Initials,
          Gender: element._fields[0].properties.Gender,
          DateOfBirth: element._fields[0].properties.DateOfBirth,
          Data: {
            _insurerId: element._fields[1].properties._insurerId,
            CompanyName: element._fields[1].properties.CompanyName,
            ZipCode: element._fields[1].properties.ZipCode,
          },
        });
      } else {
        return res.status(404).send({
          ok: false,
          status: 404,
          message:
            "Failed to retrieve user entity. Reason: User entity does not exist.",
        });
      }
    } else {
      return res.status(422).send({
        ok: false,
        status: 422,
        message:
          "Failed to process user entity. Reason: ID parameter is empty.",
      });
    }
  } catch (_e) {
    return res.status(500).send({
      ok: false,
      status: 500,
      message: `Failed to retrieve user entity. Reason: Server error ${_e.message}`,
    });
  }
});
usersRouter.get("/:id/workswith", async (req: Request, res: Response) => {
  try {
    if (req.params.id) {
      const result = await graphInstance.cypher(
        `MATCH (u1:User)<-[:WORKSWITH]-(u2)-[:HASDATA]-(d) WHERE id(u1) = ${req.params.id} RETURN u2, d`,
        {}
      );
      if (result.records.length > 0) {
        const users: any[] = [];

        for (let i = 0; i < result.records.length; i++) {
          const element: any = result.records[i];
          users.push({
            id: element._fields[0].identity.low,
            UserName: element._fields[0].properties.UserName,
            Initials: element._fields[0].properties.Initials,
            Gender: element._fields[0].properties.Gender,
            DateOfBirth: element._fields[0].properties.DateOfBirth,
            Data: {
              _insurerId: element._fields[1].properties._insurerId,
              CompanyName: element._fields[1].properties.CompanyName,
              ZipCode: element._fields[1].properties.ZipCode,
            },
          });
        }
        return res.status(200).send(users);
      } else {
        return res.status(404).send({
          ok: false,
          status: 404,
          message:
            "Failed to retrieve workswith relationship entities. Reason: No relations of this type on this user entity.",
        });
      }
    } else {
      return res.status(422).send({
        ok: false,
        status: 422,
        message:
          "Failed to process user entity. Reason: ID parameter is empty.",
      });
    }
  } catch (_e) {
    return res.status(500).send({
      ok: false,
      status: 500,
      message: `Failed to retrieve user entity. Reason: Server error ${_e.message}`,
    });
  }
});
usersRouter.get("/:id/livescloseto", async (req: Request, res: Response) => {
  try {
    if (req.params.id) {
      const result = await graphInstance.cypher(
        `MATCH (u1:User)<-[:LIVESCLOSETO]-(u2)-[:HASDATA]-(d) WHERE id(u1) = ${req.params.id} RETURN u2, d`,
        {}
      );
      if (result.records.length > 0) {
        const users: any[] = [];

        for (let i = 0; i < result.records.length; i++) {
          const element: any = result.records[i];
          users.push({
            id: element._fields[0].identity.low,
            UserName: element._fields[0].properties.UserName,
            Initials: element._fields[0].properties.Initials,
            Gender: element._fields[0].properties.Gender,
            DateOfBirth: element._fields[0].properties.DateOfBirth,
            Data: {
              _insurerId: element._fields[1].properties._insurerId,
              CompanyName: element._fields[1].properties.CompanyName,
              ZipCode: element._fields[1].properties.ZipCode,
            },
          });
        }
        return res.status(200).send(users);
      } else {
        return res.status(404).send({
          ok: false,
          status: 404,
          message:
            "Failed to retrieve workswith relationship entities. Reason: No relations of this type on this user entity.",
        });
      }
    } else {
      return res.status(422).send({
        ok: false,
        status: 422,
        message:
          "Failed to process user entity. Reason: ID parameter is empty.",
      });
    }
  } catch (_e) {
    return res.status(500).send({
      ok: false,
      status: 500,
      message: `Failed to retrieve user entity. Reason: Server error ${_e.message}`,
    });
  }
});
usersRouter.post("/", async (req: Request, res: Response) => {
  try {
    const { UserName, Initials, Gender, DateOfBirth, PasswordHash, Data } =
      req.body;
    if (UserName && Initials && Gender && DateOfBirth && PasswordHash && Data) {
      const { _insurerId, CompanyName, ZipCode } = Data;
      if (_insurerId && CompanyName && ZipCode) {
        const result = await graphInstance.cypher(
          `CREATE (us1:User{UserName: "${UserName}", Initials: "${Initials}", Gender: "${Gender}", DateOfBirth: datetime("${DateOfBirth}"), PasswordHash: "${PasswordHash}", LoginDate: datetime("${moment(
            new Date()
          ).format("yyy-MM-DTH:mm:ss")}")})` +
            `CREATE (in1:Data{_insurerId: "${_insurerId}", CompanyName: "${CompanyName}", ZipCode: "${ZipCode}"})` +
            "CREATE (us1)-[:HASDATA]->(in1)" +
            "RETURN us1, in1",
          {}
        );
        if (result.records.length > 0) {
          const element: any = result.records[0];
          const resultBody = {
            id: element._fields[0].identity.low,
            UserName: element._fields[0].properties.UserName,
            Initials: element._fields[0].properties.Initials,
            Gender: element._fields[0].properties.Gender,
            DateOfBirth: element._fields[0].properties.DateOfBirth,
            Data: {
              _insurerId: element._fields[1].properties._insurerId,
              CompanyName: element._fields[1].properties.CompanyName,
              ZipCode: element._fields[1].properties.ZipCode,
            },
          };
          const setWorksWithOperationResult = await graphInstance.cypher(
            `call apoc.periodic.iterate("
                MATCH (user:User) WHERE id(user) = ${resultBody.id}
                MATCH (d:Data)<-[:HASDATA]-(u:User)
                WHERE d.CompanyName = '${resultBody.Data.CompanyName}' AND u <> user
                RETURN user, u
                ","
                MERGE (user)-[:WORKSWITH]->(u)
                MERGE (u)-[:WORKSWITH]->(user)
              ",{batchSize:10000, parallel:false})`,
            {}
          );
          if (setWorksWithOperationResult) {
            const trimmedZipCode = resultBody.Data.ZipCode.replace(/\D+/g, "");
            const setLivesCloseToOperationResult = await graphInstance.cypher(
              `call apoc.periodic.iterate("
                  MATCH (user:User) WHERE id(user) = ${resultBody.id}
                  MATCH (d:Data)<-[:HASDATA]-(u:User)
                  WHERE d.ZipCode =~ '${trimmedZipCode}.*' AND u <> user
                  RETURN user, u
                  ","
                  MERGE (user)-[:LIVESCLOSETO]->(u)
                  MERGE (u)-[:LIVESCLOSETO]->(user)
                ",{batchSize:10000, parallel:false})`,
              {}
            );
            if (setLivesCloseToOperationResult) {
              return res.status(201).send(resultBody);
            } else {
              return res.status(404).send({
                ok: false,
                status: 404,
                message:
                  "Failed to create user entity. Reason: The cypher operation returned no records.",
              });
            }
          } else {
            return res.status(404).send({
              ok: false,
              status: 404,
              message:
                "Failed to create user entity. Reason: The cypher operation returned no records.",
            });
          }
        } else {
          return res.status(404).send({
            ok: false,
            status: 404,
            message:
              "Failed to create user entity. Reason: The cypher operation returned no records.",
          });
        }
      } else {
        return res.status(422).send({
          ok: false,
          status: 422,
          message:
            "Failed to create user entity. Reason: Missing required parameters.",
        });
      }
    } else {
      return res.status(422).send({
        ok: false,
        status: 422,
        message:
          "Failed to create user entity. Reason: Missing required parameters.",
      });
    }
  } catch (_e) {
    return res.status(500).send({
      ok: false,
      status: 500,
      message: `Failed to create user entity. Reason: Server error ${_e.message}`,
    });
  }
});
usersRouter.put("/:id", async (req: Request, res: Response) => {
  try {
    if (req.params.id && !isNaN(Number(req.params.id))) {
      const { UserName, Initials, Gender, DateOfBirth, PasswordHash, Data } =
        req.body;
      if (UserName && Initials && Gender && DateOfBirth && Data) {
        const { _insurerId, CompanyName, ZipCode } = Data;
        if (_insurerId && CompanyName && ZipCode) {
          const passChangeEntry = PasswordHash
            ? `, u.PasswordHash = "${PasswordHash}"`
            : "";
          const result = await graphInstance.cypher(
            ` MATCH (d:Data)<-[:HASDATA]-(u) WHERE id(u) = ${req.params.id}
              SET u.UserName = "${UserName}"${passChangeEntry}, u.Initials = "${Initials}", u.Gender = "${Gender}", u.DateOfBirth = datetime("${DateOfBirth}"), d._insurerId = "${_insurerId}", d.CompanyName = "${CompanyName}", d.ZipCode = "${ZipCode}"` +
              "RETURN u, d",
            {}
          );
          if (result.records.length > 0) {
            const element: any = result.records[0];
            const resultBody = {
              id: element._fields[0].identity.low,
              UserName: element._fields[0].properties.UserName,
              Initials: element._fields[0].properties.Initials,
              Gender: element._fields[0].properties.Gender,
              DateOfBirth: element._fields[0].properties.DateOfBirth,
              Data: {
                _insurerId: element._fields[1].properties._insurerId,
                CompanyName: element._fields[1].properties.CompanyName,
                ZipCode: element._fields[1].properties.ZipCode,
              },
            };

            const detachWorksWithRelations = await graphInstance.cypher(
              `call apoc.periodic.iterate("
                  MATCH (user:User) WHERE id(user) = ${req.params.id}
                  MATCH (d:Data)<-[:HASDATA]-(u:User)
                  MATCH (user)<-[r1:WORKSWITH]-(u)
                  WHERE u <> user
                  RETURN user, u, r1
                  ","
                  MATCH(u)<-[r2:WORKSWITH]-(us) WHERE us = user
                  DELETE r1
                  DELETE r2
              ",{batchSize:10000, parallel:false})`,
              {}
            );
            if (detachWorksWithRelations) {
              const detachLivesCloseToRelations = await graphInstance.cypher(
                `call apoc.periodic.iterate("
                    MATCH (user:User) WHERE id(user) = ${req.params.id}
                    MATCH (d:Data)<-[:HASDATA]-(u:User)
                    MATCH (user)<-[r1:LIVESCLOSETO]-(u)
                    WHERE u <> user
                    RETURN user, u, r1
                    ","
                    MATCH(u)<-[r2:LIVESCLOSETO]-(us) WHERE us = user
                    DELETE r1
                    DELETE r2
                ",{batchSize:10000, parallel:false})`,
                {}
              );
              if (detachLivesCloseToRelations) {
                const setWorksWithResult = await graphInstance.cypher(
                  `call apoc.periodic.iterate("
                    MATCH (user:User) WHERE id(user) = ${req.params.id}
                    MATCH (d:Data)<-[:HASDATA]-(u:User)
                    WHERE d.CompanyName = '${resultBody.Data.CompanyName}' AND u <> user
                    RETURN user, u
                    ","
                    MERGE (user)-[:WORKSWITH]->(u)
                    MERGE (u)-[:WORKSWITH]->(user)
                  ",{batchSize:10000, parallel:false})`,
                  {}
                );
                if (setWorksWithResult) {
                  const trimmedZipCode = resultBody.Data.ZipCode.replace(
                    /\D+/g,
                    ""
                  );
                  const setLivesCloseToOperationResult =
                    await graphInstance.cypher(
                      `call apoc.periodic.iterate("
                      MATCH (user:User) WHERE id(user) = ${req.params.id}
                      MATCH (d:Data)<-[:HASDATA]-(u:User)
                      WHERE d.ZipCode =~ '${trimmedZipCode}.*' AND u <> user
                      RETURN user, u
                      ","
                      MERGE (user)-[:LIVESCLOSETO]->(u)
                      MERGE (u)-[:LIVESCLOSETO]->(user)
                    ",{batchSize:10000, parallel:false})`,
                      {}
                    );
                  return res.status(200).send(resultBody);
                } else {
                  return res.status(404).send({
                    ok: false,
                    status: 404,
                    message:
                      "Failed to create user entity. Reason: The cypher operation returned no records.",
                  });
                }
              } else {
                return res.status(404).send({
                  ok: false,
                  status: 404,
                  message:
                    "Failed to create user entity. Reason: The cypher operation returned no records.",
                });
              }
            } else {
              return res.status(404).send({
                ok: false,
                status: 404,
                message:
                  "Failed to update user entity. Reason: The cypher operation returned no records.",
              });
            }
          } else {
            return res.status(404).send({
              ok: false,
              status: 404,
              message:
                "Failed to update user entity. Reason: No update was executed on this ID.",
            });
          }
        } else {
          return res.status(422).send({
            ok: false,
            status: 422,
            message:
              "Failed to update user entity. Reason: Missing required parameters.",
          });
        }
      } else {
        return res.status(422).send({
          ok: false,
          status: 422,
          message:
            "Failed to update user entity. Reason: Missing required parameters.",
        });
      }
    } else {
      return res.status(422).send({
        ok: false,
        status: 422,
        message: "Failed to update user entity. Reason: ID parameter is empty.",
      });
    }
  } catch (_e) {
    return res.status(500).send({
      ok: false,
      status: 500,
      message: `Failed to update user entity. Reason: Server error ${_e.message}`,
    });
  }
});
usersRouter.delete("/:id", async (req: Request, res: Response) => {
  try {
    if (req.params.id) {
      const result = await graphInstance.cypher(
        `MATCH (u:User) WHERE id(u) = ${req.params.id} DETACH DELETE u RETURN u`,
        {}
      );
      if (result.records.length > 0) {
        const element: any = result.records[0];
        return res.status(200).send({
          id: element._fields[0].identity.low,
        });
      } else {
        return res.status(404).send({
          ok: false,
          status: 404,
          message: "Failed to delete user entity. Reason: User does not exist.",
        });
      }
    } else {
      return res.status(422).send({
        ok: false,
        status: 422,
        message: "Failed to delete user entity. Reason: ID parameter is empty.",
      });
    }
  } catch (_e) {
    return res.status(500).send({
      ok: false,
      status: 500,
      message: `Failed to delete user entity. Reason: Server error ${_e.message}`,
    });
  }
});
export default usersRouter;
