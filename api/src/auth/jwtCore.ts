import { encode, decode, TAlgorithm } from "jwt-simple";
import { DecodeResult } from "./DecodeResult";
import { EncodeResult } from "./EncodeResult";
import { Session } from "./Session";
import { Request } from "express";

export type PartialSession = Omit<Session, "issued" | "expires">;
export type ExpirationStatus = "expired" | "active";

export function encodeSession(
  secretKey: string,
  partialSession: PartialSession
): EncodeResult {
  const algorithm: TAlgorithm = "HS512";
  const issued = Date.now();
  const expires = issued + 60 * 60 * 1000; // 60 minutes
  const session: Session = {
    ...partialSession,
    issued,
    expires,
  };

  return {
    token: encode(session, secretKey, algorithm),
    issued,
    expires,
  };
}

export function decodeSession(
  secretKey: string,
  tokenString: string
): DecodeResult {
  const algorithm: TAlgorithm = "HS512";
  let result: Session;
  try {
    result = decode(tokenString, secretKey, false, algorithm);
  } catch (_e) {
    const e: Error = _e;
    if (
      e.message === "No token supplied" ||
      e.message === "Not enough or too many segments"
    ) {
      return {
        type: "invalid-token",
      };
    }

    if (
      e.message === "Signature verification failed" ||
      e.message === "Algorithm not supported"
    ) {
      return {
        type: "integrity-error",
      };
    }

    if (e.message.indexOf("Unexpected token") === 0) {
      return {
        type: "invalid-token",
      };
    }

    throw e;
  }

  return {
    type: "valid",
    session: result,
  };
}

export function checkExpirationStatus(token: Session): ExpirationStatus {
  if (token.expires > Date.now()) return "active";
  return "expired";
}

export function getSessionBearerId(req: Request): number {
  const decodedSession: DecodeResult = decodeSession(
    "726c7017-b252-4f44-af9b-71ceb034b69c",
    req.header("X-JWT-Token")
  );
  if (decodedSession.type === "valid") {
    return decodedSession.session.id;
  }
  return -1;
}
