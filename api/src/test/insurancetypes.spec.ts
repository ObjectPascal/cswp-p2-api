// tslint:disable:no-unused-expression

import { expect } from "chai";
import app from "../index";
import supertest from "supertest";

const apiUrl: string = "/api/v1/insurancetypes";
const fakeId: string = "61b0b3359a6830143701e4f8";
const testUser: any = {
  UserName: "user1",
  PasswordHash:
    "a4e4570c263dbfe7e7b01c451f5121f025c7bba885bb3c373ba4a578b4a7dca4",
};

const testInsuranceType: any = {
  TypeName: "Mocha Test InsuranceType",
  PaymentPeriodMonths: 1,
};
const testInsuranceTypeIncomplete: any = {
  PaymentPeriodMonths: 1,
};

describe("> insurancetype tests", () => {
  let authToken: string = "";
  before(async () => {
    const result = await supertest(app).post("/api/auth").send(testUser);
    const { token } = result.body;
    authToken = token;
  });

  let insuranceTypeId: string = "";
  it("C) should create a correct insurancetype with a correct id", async () => {
    const result = await supertest(app)
      .post(apiUrl)
      .set("X-JWT-Token", authToken)
      .send(testInsuranceType);

    expect(result.statusCode).to.equal(201);
    expect(result.headers["content-type"]).to.equal(
      "application/json; charset=utf-8"
    );

    const { _id, _entityData, TypeName, PaymentPeriodMonths } = result.body;

    expect(_id).to.be.a("string").that.is.not.empty;
    insuranceTypeId = _id;

    expect(_entityData).to.be.a("object").that.is.not.null;
    expect(TypeName).to.be.a("string").that.equals("Mocha Test InsuranceType");
    expect(PaymentPeriodMonths).to.be.a("number").that.equals(1);
  });
  it("C) should not create an insurancetype with incomplete data", async () => {
    const result = await supertest(app)
      .post(apiUrl)
      .set("X-JWT-Token", authToken)
      .send(testInsuranceTypeIncomplete);

    expect(result.statusCode).to.equal(422);
    expect(result.headers["content-type"]).to.equal(
      "application/json; charset=utf-8"
    );
  });

  it("R) should provide a list of insurancetypes", async () => {
    const result = await supertest(app)
      .get(`${apiUrl}/list`)
      .set("X-JWT-Token", authToken);

    expect(result.statusCode).to.equal(200);
    expect(result.headers["content-type"]).to.equal(
      "application/json; charset=utf-8"
    );

    expect(result.body).to.not.be.null;
    expect(result.body).to.have.length.greaterThan(0);
    expect(result.body).to.be.a("array");
  });
  it("R) should read a correct insurancetype", async () => {
    const result = await supertest(app)
      .get(`${apiUrl}/${insuranceTypeId}`)
      .set("X-JWT-Token", authToken);

    expect(result.statusCode).to.equal(200);
    expect(result.headers["content-type"]).to.equal(
      "application/json; charset=utf-8"
    );

    const { _id, _entityData, TypeName, PaymentPeriodMonths } = result.body;

    expect(_id).to.be.a("string").that.is.not.empty;
    expect(_entityData).to.be.a("object").that.is.not.null;
    expect(TypeName).to.be.a("string").that.is.not.empty;
    expect(PaymentPeriodMonths).to.be.a("number").that.is.above(-1);
  });
  it("R) should not read an insurancetype with a false id", async () => {
    const result = await supertest(app)
      .get(`${apiUrl}/${fakeId}`)
      .set("X-JWT-Token", authToken);

    expect(result.statusCode).to.equal(404);
    expect(result.headers["content-type"]).to.equal(
      "application/json; charset=utf-8"
    );
  });

  it("U) should update a correct insurancetype with a correct id", async () => {
    const result = await supertest(app)
      .put(`${apiUrl}/${insuranceTypeId}`)
      .set("X-JWT-Token", authToken)
      .send(testInsuranceType);

    expect(result.statusCode).to.equal(200);
    expect(result.headers["content-type"]).to.equal(
      "application/json; charset=utf-8"
    );

    const { _id, _entityData, TypeName, PaymentPeriodMonths } = result.body;

    expect(_id).to.be.a("string").that.is.not.empty;
    expect(_entityData).to.be.a("object").that.is.not.null;
    expect(TypeName).to.be.a("string").that.is.not.empty;
    expect(PaymentPeriodMonths).to.be.a("number").that.is.above(-1);
  });
  it("U) should not update an insurancetype with a false id", async () => {
    const result = await supertest(app)
      .put(`${apiUrl}/${fakeId}`)
      .set("X-JWT-Token", authToken)
      .send(testInsuranceType);

    expect(result.statusCode).to.equal(404);
    expect(result.headers["content-type"]).to.equal(
      "application/json; charset=utf-8"
    );
  });
  it("U) should not update an insurancetype with incomplete data", async () => {
    const result = await supertest(app)
      .put(`${apiUrl}/${insuranceTypeId}`)
      .set("X-JWT-Token", authToken)
      .send(testInsuranceTypeIncomplete);

    expect(result.statusCode).to.equal(422);
    expect(result.headers["content-type"]).to.equal(
      "application/json; charset=utf-8"
    );
  });

  it("D) should delete and return data when a correct insurancetype with a correct id is deleted", async () => {
    const result = await supertest(app)
      .delete(`${apiUrl}/${insuranceTypeId}`)
      .set("X-JWT-Token", authToken);

    expect(result.statusCode).to.equal(200);
    expect(result.headers["content-type"]).to.equal(
      "application/json; charset=utf-8"
    );

    const { _id, _entityData, TypeName, PaymentPeriodMonths } = result.body;

    expect(_id).to.be.a("string").that.is.not.empty;
    expect(_entityData).to.be.a("object").that.is.not.null;
    expect(TypeName).to.be.a("string").that.is.not.empty;
    expect(PaymentPeriodMonths).to.be.a("number").that.is.above(-1);
  });
  it("D) should not delete an insurancetype if it is already deleted", async () => {
    const result = await supertest(app)
      .delete(`${apiUrl}/${insuranceTypeId}`)
      .set("X-JWT-Token", authToken);

    expect(result.statusCode).to.equal(404);
    expect(result.headers["content-type"]).to.equal(
      "application/json; charset=utf-8"
    );
  });
  it("D) should not delete an insurancetype with a fake id", async () => {
    const result = await supertest(app)
      .delete(`${apiUrl}/${fakeId}`)
      .set("X-JWT-Token", authToken);

    expect(result.statusCode).to.equal(404);
    expect(result.headers["content-type"]).to.equal(
      "application/json; charset=utf-8"
    );
  });
});
