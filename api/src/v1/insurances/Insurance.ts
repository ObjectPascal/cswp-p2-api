import mongoose from "mongoose";

interface IInsurance {
  _entityData: object;
  _insurer: mongoose.Types.ObjectId;
  _insuranceType: mongoose.Types.ObjectId;
  _insurancePolicy: mongoose.Types.ObjectId;
  PaymentPeriodMonths: number;
  PrevCollectionDate: Date;
  NextCollectionDate: Date;
  Description: string;
}

interface InsuranceModelInterface extends mongoose.Model<InsuranceDoc> {
  build(data: IInsurance): InsuranceDoc;
}

interface InsuranceDoc extends mongoose.Document {
  _entityData: object;
  _insurer: mongoose.Types.ObjectId;
  _insuranceType: mongoose.Types.ObjectId;
  _insurancePolicy: mongoose.Types.ObjectId;
  PaymentPeriodMonths: number;
  PrevCollectionDate: Date;
  NextCollectionDate: Date;
  Description: string;
}

const schema = new mongoose.Schema(
  {
    _entityData: {
      _changedById: Number,
      LastChanged: {
        type: Date,
        default: new Date(Date.now()),
      },
    },
    _insurer: {
      type: mongoose.Types.ObjectId,
      ref: "Insurer",
      required: true,
    },
    _insuranceType: {
      type: mongoose.Types.ObjectId,
      ref: "InsuranceType",
      required: true,
    },
    _insurancePolicy: {
      type: mongoose.Types.ObjectId,
      ref: "InsurancePolicy",
      required: true,
    },
    PaymentPeriodMonths: {
      type: Number,
      required: true,
    },
    PrevCollectionDate: {
      type: Date,
      required: true,
    },
    NextCollectionDate: {
      type: Date,
      required: true,
    },
    Description: {
      type: String,
      required: true,
    },
  },
  {
    versionKey: false,
  }
);

schema.statics.build = (data: IInsurance) => {
  return new Insurance(data);
};

const Insurance = mongoose.model<InsuranceDoc, InsuranceModelInterface>(
  "Insurance",
  schema
);
export { Insurance };
